<?php
/**
 * index.php
 *
 * Boostrap file for Online Wine Warehouse website
 *
 * Group 30
 * CI6230 Advanced Databases and Web
 * Kingston University
 */

$page_title = 'Online Wine Warehouse';
// Path for js and css files
$path = '';
$view_permission = 'public';


// -- HEADER
require_once('../load.php');


//-- NAVBAR 
require_once($path . '../layout/navigation.php');
?>

    <!-- Content -->
    <div class="container admin">
        <div class="row">
            <div class="col-sm-9">
                <ol class="breadcrumb">
                    <li><a href="site.php">Home</a></li>
                    <li class="active">
                        <?php
                        echo($page['title']);

                        if (isset($_POST['search'])) {
                            echo 'Search';
                        }
                        ?>
                    </li>
                </ol>
                <?php echo form_errors(); ?>
                <h2><?php echo($page['title']); ?></h2>

                <p>
                    <?php
                    show_search_results('wine');

                    if (isset($page['content'])) {
                        echo($page['content']);
                    }
                    // Get related PHP file name from database- file_name column and include it
                    if (file_exists($path . 'page/' . $page['file_name'] . '.php')) {
                        include_once($path . 'page/' . $page['file_name'] . '.php');
                    }
                    ?>
                </p>
            </div>
            <!-- /.col-sm-9 -->
            <!-- Sidebar -->
            <?php require_once($path . '../layout/sidebar.php'); ?>
        </div>
        <!-- /.row -->

        <!-- START THE FEATURETTES -->
        <hr class="featurette-divider">

        <div class="row featurette">
            <div class="col-md-7">
                <h2 class="featurette-heading">Bordeaux <span class="text-muted"> The great 2010 vintage produced some spectacular wines</span>
                </h2>

                <p class="lead">With over 10,000 properties, Bordeaux is a veritable treasure trove of fine and everyday
                    drinking. Styles range from modern to traditional, dry or sweet whites to easy-drinking and serious
                    cellar-worthy reds. We've sifted through the many bottles available to find members lovely wines
                    that punch above their weight</p>
            </div>
            <div class="col-md-5">
                <img class="featurette-image img-responsive" src="<?php echo $path; ?>images/wine-making.jpg"
                     alt="Generic placeholder image">
            </div>
        </div>

        <hr class="featurette-divider">
        <!-- /END THE FEATURETTES -->

        <!-- FOOTER -->
        <footer>
            <p class="pull-right"><a href="#">Back to top</a></p>

            <p>&copy; 2014 Online Wine Warehouse, Inc. &middot; <a href="#">Privacy</a> &middot; <a href="#">Terms</a>
            </p>
        </footer>

    </div><!-- /.container -->

<?php
require_once('../layout/footer.php');
