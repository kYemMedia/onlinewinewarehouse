<?php
/**
 *
 * Boostrap file for Online Wine Warehouse website backend
 *
 * Group 30
 * CI6230 Advanced Databases and Web
 * Kingston University
 *
 */
$page_title = 'OWW - Admin';
// Path for js and css files
$path = '';
$view_permission = 'admin';

// -- HEADER
require_once('../load.php' );

//-- NAVBAR
require_once(ABSPATH.'/layout/admin-navigation.php' );
  ?>

   <!-- Content -->
    <div class="container admin">
      <div class="row">
        <div class="col-sm-9">
           <?php if (isset($_GET['success'])){echo alert_msg($_GET['success']);}
          ?>
          <h2><?php echo($page['title']);?></h2>
          <p><?php
            echo ($page['content']);
            ?></p>
          <h1>Admin Dashboard</h1>
          <h2><span class="text-muted"> Please select a page</span></h2>

        </div><!-- /.col-sm-9 -->
      </div><!-- /.row -->

      <!-- START THE FEATURETTES -->
      <hr class="featurette-divider">

      <div class="row featurette">
        <div class="col-md-7">
          <h2 class="featurette-heading">Discover Good Wine. <span class="text-muted">An extensive hand-picked range from the classics to the quirky.</span></h2>
          <p class="lead">High-quality wines at all prices. Around 1,500 wines listed with over 200 of them between £5 and £8 a bottle</p>
        </div>
        <div class="col-md-5">
          <img class="featurette-image img-responsive" src="images/botllegalss.jpg" alt="Generic placeholder image">
        </div>
      </div>

      <hr class="featurette-divider">
      <!-- /END THE FEATURETTES -->

      <!-- FOOTER -->
      <footer>
        <p class="pull-right"><a href="#">Back to top</a></p>
        <p>&copy; 2014 Online Wine Warehouse, Inc. &middot; <a href="#">Privacy</a> &middot; <a href="#">Terms</a></p>
      </footer>

    </div><!-- /.container -->

<?php
require_once('../layout/footer.php');
