<?php
// Page Title
$page_title = 'Login';
// Table name to get all information
$table_name = 'customer';
$view_permission = 'public';
// Path for js and css files
$path = '';
$username = '';

require_once($path . '../helper/db.php');
require_once($path . '../helper/functions.php');
session_start();
$page = getCurrentPage();

checkAccessPermissions($view_permission, $path);


if (isset($_POST['submit'])) {
    // Escape strings
    $username = mysqli_escape_string($mysqli, $_POST["username"]);
    $password = $_POST["password"];

    // validations
    $required_fields = array('username', 'password');
    validate_presences($required_fields);

    $fields_with_max_lengths = array('username' => 30, 'password' => 32);
    validate_max_lengths($fields_with_max_lengths);

    if (!empty($errors)) {
        $_SESSION["errors"] = $errors;
    } else {
        $found_user = attempt_login($username, $password, 'customer');

        if ($found_user) {

            $_SESSION['user_id'] = $found_user['CUST_ID'];
            $_SESSION['username'] = $found_user['USERNAME'];
            $_SESSION['auth'] = 1;
            if (isset($_SESSION['shopping_cart']['order_url'])) {
                redirect_to($_SESSION['shopping_cart']['order_url']);
            } else {
                redirect_to($path . 'index.php');
            }
        } else {
            $_SESSION['message'] = 'Username or password do not match';
            $_SESSION['message_type'] = 'warning';
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <!-- <meta http-equiv="X-UA-Compatible" content="IE=edge"> -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Online Wine Warehouse - A wide range of wines available online">
        <meta name="author" content="Group 30">
        <link rel="shortcut icon" href="<?php echo $path; ?>images/ico/favicon.ico">

        <title><?php echo $page_title; ?></title>

        <!-- Bootstrap core CSS -->
        <link href="<?php echo $path; ?>css/bootstrap.css" rel="stylesheet">
        <link href="<?php echo $path; ?>css/datepicker3.css" rel="stylesheet">
        <!-- Custom styles -->
        <link href="<?php echo $path; ?>css/carousel.css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script src="<?php echo $path; ?>js/bootstrap.min.js"></script>
        <script src="<?php echo $path; ?>js/docs.min.js"></script>
        <script src="<?php echo $path; ?>js/bootstrap-datepicker.js"></script>

    </head>

<body>
<?php
require_once($path . '../layout/navigation.php');
?>
    <!-- Content -->
    <div class="container admin">
        <div class="row">
            <div class="col-sm-9">
                <?php echo form_errors(); ?>
                <h2><?php echo($page['title']);?></h2>

                <form method="post" class="form-horizontal" role="form">
                    <div class="form-group">
                        <label for="username" class="required col-sm-2 control-label">Username <span
                                class="required">*</span></label>

                        <div class="col-xs-4">
                            <input type="text" class="form-control" id="username" name="username" placeholder="Username"
                                   autocomplete="off" required="" autofocus=""
                                   value="<?php echo htmlentities($username) ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password" class="required col-sm-2 control-label">Password<span
                                class="required">*</span></label>

                        <div class="col-xs-4">
                            <input type="password" id="password" class="form-control" name="password" autocomplete="off"
                                   placeholder="Password" required="">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" name="submit" value="submit" class="btn btn-default">Log In</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.col-sm-9 -->
            <!-- Sidebar -->
            <?php require_once($path . '../layout/sidebar.php'); ?>
        </div>
        <!-- /.row -->


        <!-- START THE FEATURETTES -->

        <hr class="featurette-divider">

        <div class="row featurette">
            <div class="col-md-7">
                <h2 class="featurette-heading">Bordeaux <span class="text-muted"> The great 2010 vintage produced some spectacular wines</span>
                </h2>

                <p class="lead">With over 10,000 properties, Bordeaux is a veritable treasure trove of fine and everyday
                    drinking. Styles range from modern to traditional, dry or sweet whites to easy-drinking and serious
                    cellar-worthy reds. We've sifted through the many bottles available to find members lovely wines
                    that punch above their weight</p>
            </div>
            <div class="col-md-5">
                <img class="featurette-image img-responsive" src="<?php echo $path; ?>images/wine-making.jpg"
                     alt="Generic placeholder image">
            </div>
        </div>

        <hr class="featurette-divider">
        <!-- /END THE FEATURETTES -->


        <!-- FOOTER -->
        <footer>
            <p class="pull-right"><a href="#">Back to top</a></p>

            <p>&copy; 2014 Online Wine Warehouse, Inc. &middot; <a href="#">Privacy</a> &middot; <a href="#">Terms</a>
            </p>
        </footer>

    </div><!-- /.container -->

<?php
require_once($path . '../layout/footer.php');
