<?php
/**
 * index.php
 *
 * Boostrap file for Online Wine Warehouse website
 *
 * Group 30
 * CI6230 Advanced Databases and Web
 * Kingston University
 */
$page_title = 'Online Wine Warehouse - Home';
// Path for js and css files
$path = '';
$view_permission = 'public';


// -- HEADER
require_once('../load.php' );

//-- NAVBAR
require_once(ABSPATH . 'layout/navigation.php')
?>
    <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner">
        <div class="item active">
          <img src="images/wine-slide-004.jpg" alt="First slide">
          <div class="container">
            <div class="carousel-caption">
              <h1>Online Wine Warehouse</h1>
              <p>Rere top wines are available to buy online.</p>
              <p><a class="btn btn-lg btn-success" href="site.php?subject=26" role="button">Browse Wines</a></p>
            </div>
          </div>
        </div>
        <div class="item">
          <img src="images/wine-slide-003.jpg" alt="First slide">
        </div>
        <div class="item">
          <img src="images/wine-slide-002.jpg" alt="First slide">
          
        </div>
      </div>
      <a class="left carousel-control" href="#myCarousel" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
      <a class="right carousel-control" href="#myCarousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
    </div><!-- /.carousel -->



    <!-- Marketing messaging and featurettes
    ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->

    <div class="container marketing">
        <?php  echo form_errors(); ?>
      <!-- Three columns of text below the carousel -->
      <div class="row">
        <div class="col-lg-4">

          <img class="img-circle" src="images/wine-001.jpg" alt="Generic placeholder image">
          <h2>Red Wine</h2>
          <p>Red wine is a type of wine made from dark-coloured grape varieties. Still wine made with black grapes. These can range from light to dark and bone-dry to sweet.</p>
          <p><a class="btn btn-default" href="site.php?page=5" role="button">View details &raquo;</a></p>
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
          <img class="img-circle" src="images/white-wine-pour-white-1008625261.jpg" alt="Generic placeholder image">
          <h2>White Wine</h2>
          <p>White wine is a wine whose colour can be straw-yellow, yellow-green, or yellow-gold coloured. It is produced by the alcoholic fermentation of the non-coloured pulp of grapes.</p>
          <p><a class="btn btn-default" href="site.php?page=6" role="button">View details &raquo;</a></p>
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
          <img class="img-circle" src="images/cheers_2_champagne_glasses_clinking.jpg" alt="Generic placeholder image">
          <h2>Champagne</h2>
          <p>A style of winemaking involving a secondary fermetation causing bubbles! Sparkling wine can be red, white or rosé and can range from minerally to rich and sweet</p>
          <p><a class="btn btn-default" href="site.php?page=8" role="button">View details &raquo;</a></p>
        </div><!-- /.col-lg-4 -->
      </div><!-- /.row -->


      <!-- START THE FEATURETTES -->

      <hr class="featurette-divider">

      <div class="row featurette">
        <div class="col-md-7">
          <h2 class="featurette-heading">Discover Good Wine. <span class="text-muted">An extensive hand-picked range from the classics to the quirky.</span></h2>
          <p class="lead">High-quality wines at all prices. Around 1,500 wines listed with over 200 of them between £5 and £8 a bottle</p>
        </div>
        <div class="col-md-5">
          <img class="featurette-image img-responsive" src="images/botllegalss.jpg" alt="Generic placeholder image">
        </div>
      </div>

      <hr class="featurette-divider">

      <div class="row featurette">
        <div class="col-md-5">
          <img class="featurette-image img-responsive" src="images/red-wine-bottles-stacked-71718745-ss.jpg" alt="Generic placeholder image">
        </div>
        <div class="col-md-7">
          <h2 class="featurette-heading">Mature Wines <span class="text-muted">Purchases for a special anniversary.</span></h2>
          <p class="lead">Wine matures more quickly in halves than in standard bottles, and more slowly in magnums and larger formats, a useful tip both for early drinkers and advance planners wanting to maximise the life of their purchases for a distant coming of age or special anniversary. Just as half-bottles add variety to intimate dinners, a magnum makes an impressive statement for a larger party. Divided into two standard decanters, it will also keep both ends of a big table happy.</p>
        </div>
      </div>

      <hr class="featurette-divider">

      <div class="row featurette">
        <div class="col-md-7">
          <h2 class="featurette-heading">Bordeaux <span class="text-muted"> The great 2010 vintage produced some spectacular wines</span></h2>
          <p class="lead">With over 10,000 properties, Bordeaux is a veritable treasure trove of fine and everyday drinking. Styles range from modern to traditional, dry or sweet whites to easy-drinking and serious cellar-worthy reds. We've sifted through the many bottles available to find members lovely wines that punch above their weight</p>
        </div>
        <div class="col-md-5">
          <img class="featurette-image img-responsive" src="<?php echo $path; ?>images/wine-making.jpg" alt="Generic placeholder image">
        </div>
      </div>

      <hr class="featurette-divider">

      <!-- /END THE FEATURETTES -->


      <!-- FOOTER -->
      <footer>
        <p class="pull-right"><a href="#">Back to top</a></p>
        <p>&copy; 2014 Online Wine Warehouse, Inc. &middot; <a href="#">Privacy</a> &middot; <a href="#">Terms</a></p>
      </footer>

    </div><!-- /.container -->

<?php 
require('../layout/footer.php');
