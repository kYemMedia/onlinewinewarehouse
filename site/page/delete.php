<?php
/**
 * Form Page
 *
 * Group 30
 * CI6230 Advanced Databases and Web
 * Kingston University
 */
$page_title = 'Update Subject';

// Path for js and css files
$path = '../';
// -- Helpers
require_once($path . '../helper/db.php');
require_once($path . '../helper/functions.php');
session_start();


if (isset($_POST['id']) && isset($_POST['table']) && isset($_POST['column_name'])) {
    $id = (int)$_POST['id'];
    $table = mysqli_escape_string($mysqli, $_POST['table']);
    $column_name = mysqli_escape_string($mysqli, $_POST['column_name']);

    // Check if it is composite key (two columns)
    if (isset($_POST['column_name_2']) && isset($_POST['id_2'])) {
        $id_2 = (int)$_POST['id_2'];
        $column_name_2 = mysqli_escape_string($mysqli, $_POST['column_name_2']);
        $query = "DELETE FROM {$table} ";
        $query .= "WHERE {$column_name}='{$id}' ";
        $query .= "AND {$column_name_2}='{$id_2}' ";
        $query .= "LIMIT 1";
    } else {

        $query = "DELETE FROM {$table} ";
        $query .= "WHERE {$column_name}='{$id}' ";
        $query .= "LIMIT 1";
    }

    if ($mysqli->query($query)) {
        $_SESSION['message'] = 'Success! ' . fieldNameAsText($table) . ' was deleted!';
        $_SESSION['message_type'] = 'success';
        redirect_to('form-' . $table . '-view.php');
    } else {
        $_SESSION['message'] = "Error. Distribution Centre creation failed: (" . $mysqli->errno . ") " . $mysqli->error;
        $_SESSION['message_type'] = 'warning';
        redirect_to('form-' . $table . '-view.php');
    }
}
