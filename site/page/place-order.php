<?php
/**
 * Form Page
 *
 * Group 30
 * CI6230 Advanced Databases and Web
 * Kingston University
 */
$page_title = 'Place Order';
// Path for js and css files
$path = '../';
$view_permission = 'place-order';

// -- HEADER
require_once($path . '../load.php');


if (isset($_SESSION['wines'])) {
    $customer_id = (int)$_SESSION['user_id'];

    if (isset($_SESSION['shopping_cart'])) {
        $delivery_charge = $_SESSION['shopping_cart']['shipping'];
        $order_total = $_SESSION['shopping_cart']['order_total'];
    } else {
        $delivery_charge = 0;
        $order_total = 0;
    }

    // Check if there is any errors
    if (!empty($errors)) {
        $_SESSION["errors"] = $errors;
    } else {

        // Build query for Customer Order
        $query = "INSERT INTO `cust_order` ";
        $query .= "(`CUST_ORD_ID`, `CUST_ID`, `ORDER_DATE`, `DELIVERY_CHARGE`, `total`) ";
        $query .= "VALUES (NULL, {$customer_id}, NOW(), {$delivery_charge}, {$order_total});";

        if ($mysqli->query($query)) {
            $order_id = mysqli_insert_id($mysqli);

            // Build sub_query for the order item
            foreach ($_SESSION['wines'] as $wine_id => $wine_array) {
                $sub_query = "INSERT INTO `order_item` ";
                $sub_query .= "(`WINE_ID`, `CUST_ORD_ID`, `QUANTITY`, `CASE_QUANTITY`) ";
                $sub_query .= "VALUES ({$wine_array['id']}, {$order_id}, ";
                $sub_query .= "{$wine_array['quantity']['bottle_quantity']}, ";
                $sub_query .= "{$wine_array['quantity']['case_quantity']} );";
                // If there is errors show them
                if (!$mysqli->query($sub_query)) {
                    $_SESSION['message'] = "There was an error with items, please contact customer service team";
                    $_SESSION['message_type'] = 'warning';
                    redirect_to($path . 'site.php?subject=11');
                } else {
                    // Everything was executed successfully
                    $_SESSION['message'] = 'Success! Your order have been placed!';
                    $_SESSION['message_type'] = 'success';

                    // Clear Shopping Cart
                    $_SESSION['wines'] = null;
                    $_SESSION['shopping_cart'] = null;

                    // Redirect to wine catalog
                    redirect_to($path . 'site.php?subject=26');
                }
            }
            // Unable to create new customer order
        } else {
            $_SESSION['message'] = "There was an error with customer order, please contact customer service team " .$mysqli->error;
            $_SESSION['message_type'] = 'warning';
            redirect_to($path . 'site.php?subject=11');
        }
    } // No Error Loop
} // if (isset($_SESSION['wines']))

//-- NAVBAR
require_once($path . '../layout/navigation.php');
?>

    <!-- Content -->
    <div class="container admin">
        <div class="row">
            <div class="col-sm-9">
                <?php echo form_errors(); ?>
                <ol class="breadcrumb">
                    <li><a href="../site.php">Home</a></li>
                    <li class="active"><?php echo $page_title; ?></li>
                </ol>
                <h2 class="col-sm-offset-2"><?php echo $page_title; ?></h2>

            </div>
            <!-- content -->
            <!-- Sidebar -->

        </div>
        <!-- /.row -->


        <!-- FOOTER -->
        <footer>
            <p class="pull-right"><a href="#">Back to top</a></p>

            <p>&copy; 2014 Online Wine Warehouse, Inc. &middot; <a href="#">Privacy</a> &middot; <a href="#">Terms</a>
            </p>
        </footer>

    </div><!-- /.container -->

<?php
require_once($path . '../layout/footer.php');