<?php

require_once '../../helper/db.php';
require_once '../../helper/functions.php';
session_start();

if (isset($_SESSION['username']) && isset($_SESSION['auth'])) {

    $_SESSION['username'] = null;
    $_SESSION['user_id'] = null;
    $_SESSION['auth'] = null;
}
redirect_to('../index.php');
