<?php
/**
 * Form Page
 *
 * Group 30
 * CI6230 Advanced Databases and Web
 * Kingston University
 */

// Page Title
$page_title = 'Update Distribution Centre';
$table_name = 'distribution_centre';
// Path for js and css files
$path = '../';
// -- HEADER
require_once($path . '../load.php');

// Init values to avoid undefined notice
$distribution_centre_id = null;
$name = null;
$street = null;
$town = null;
$postcode = null;
$telephone = null;

if (isset($_GET['id'])) {
    $id = mysqli_escape_string($mysqli, $_GET['id']);
    $dist_centre = find_dist_centre_by_id($id);

    $name = $dist_centre['NAME'];
    $street = $dist_centre['STREET'];
    $town = $dist_centre['TOWN'];
    $postcode = $dist_centre['POSTCODE'];
    $telephone = $dist_centre['TELEPHONE'];


    // If form was submited
    if (isset($_POST['submit'])) {

        // Escape strings, type cast int
        $distribution_centre_id = (int)$_POST["DIST_CENTRE_ID"];
        $name = mysqli_escape_string($mysqli, $_POST["NAME"]);
        $street = mysqli_escape_string($mysqli, $_POST["STREET"]);
        $town = mysqli_escape_string($mysqli, $_POST["TOWN"]);
        $postcode = mysqli_escape_string($mysqli, $_POST["POSTCODE"]);
        $telephone = mysqli_escape_string($mysqli, $_POST["TELEPHONE"]);

        // validations
        $required_fields = array('DIST_CENTRE_ID', 'NAME', 'STREET', 'TOWN', 'POSTCODE', 'TELEPHONE');
        validate_presences($required_fields);

        $fields_with_max_lengths = array(
            'DIST_CENTRE_ID' => 3,
            'NAME'           => 15,
            'STREET'         => 15,
            'TOWN'           => 15,
            'POSTCODE'       => 7,
            'TELEPHONE'      => 50
        );
        validate_max_lengths($fields_with_max_lengths);

        // Check if there is any errors
        if (!empty($errors)) {
            $_SESSION["errors"] = $errors;
        } else {

            // Build query
            $query = "UPDATE `{$table_name}` ";
            $query .= "SET `dist_centre_id`={$distribution_centre_id},`name`= '{$name}', `street`= '{$street}', `town`= '{$town}', ";
            $query .= "`postcode`= '{$postcode}',`telephone` = '{$telephone}'";
            $query .= "WHERE `dist_centre_id`={$id} ";
            $query .= "Limit 1 ";
            saveInformation($query, $table_name);
        }
    }
}
//-- NAVBAR
require_once($path . '../layout/admin-navigation.php');
?>
    <!-- Content -->
    <div class="container admin">
        <div class="row">
            <div class="col-sm-12">
                <?php echo form_errors(); ?>
                <ol class="breadcrumb">
                    <li><a href="../site.php">Home</a></li>
                    <li class="active"><?php echo $page_title; ?></li>
                </ol>
                <h2 class="col-sm-offset-2"><?php echo $page_title; ?></h2>

                <div>
                    <form method="post" class="form-horizontal">
                        <div class="form-group">
                            <label for="DIS_CENTRE_ID" class="required col-sm-2 control-label">Id <span
                                    class="required">*</span></label>

                            <div class="col-xs-4">
                                <input maxlength="45" id="DIS_CENTRE_ID" class="form-control" name="DIST_CENTRE_ID"
                                       type="text" value="<?php echo $distribution_centre_id ?>"></div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">Name</label>

                            <div class="col-xs-4">
                                <input id="name" class="form-control" name="NAME" type="text"
                                       value="<?php echo $name ?>"></div>
                        </div>

                        <div class="form-group">
                            <label for="STREET" class="col-sm-2 control-label">Street</label>

                            <div class="col-xs-4">
                                <input id="STREET" class="form-control" name="STREET" type="text"
                                       value="<?php echo $street ?>"></div>
                        </div>

                        <div class="form-group">
                            <label for="TOWN" class="col-sm-2 control-label">Town</label>

                            <div class="col-xs-4">
                                <input class="form-control" name="TOWN" id="TOWN" type="text"
                                       value="<?php echo $town ?>"></div>
                        </div>

                        <div class="form-group">
                            <label for="POSTCODE" class="col-sm-2 control-label">Postcode</label>

                            <div class="col-xs-4">
                                <input class="form-control" name="POSTCODE" id="POSTCODE" type="text"
                                       value="<?php echo $postcode ?>"></div>
                        </div>
                        <div class="form-group">
                            <label for="TELEPHONE" class="col-sm-2 control-label">Telephone</label>

                            <div class="col-xs-4">
                                <input class="form-control" name="TELEPHONE" id="TELEPHONE" type="text"
                                       value="<?php echo $telephone ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" name="submit" class="btn btn-default">Update</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- content -->


        </div>
        <!-- /.row -->


        <!-- START THE FEATURETTES -->

        <hr class="featurette-divider">

        <div class="row featurette">
            <div class="col-md-7">
                <h2 class="featurette-heading">Bordeaux <span class="text-muted"> The great 2010 vintage produced some spectacular wines</span>
                </h2>

                <p class="lead">With over 10,000 properties, Bordeaux is a veritable treasure trove of fine and everyday
                    drinking. Styles range from modern to traditional, dry or sweet whites to easy-drinking and serious
                    cellar-worthy reds. We've sifted through the many bottles available to find members lovely wines
                    that punch above their weight</p>
            </div>
            <div class="col-md-5">
                <img class="featurette-image img-responsive" src="<?php echo $path; ?>images/wine-making.jpg"
                     alt="Generic placeholder image">
            </div>
        </div>

        <hr class="featurette-divider">
        <!-- /END THE FEATURETTES -->


        <!-- FOOTER -->
        <footer>
            <p class="pull-right"><a href="#">Back to top</a></p>

            <p>&copy; 2014 Online Wine Warehouse, Inc. &middot; <a href="#">Privacy</a> &middot; <a href="#">Terms</a>
            </p>
        </footer>

    </div><!-- /.container -->

<?php
require_once($path . '../layout/footer.php');