<?php
/**
 * Form Page
 *
 * Group 30
 * CI6230 Advanced Databases and Web
 * Kingston University
 */

// Page Title
$page_title = 'Admin Login';
// Table name to get all information
$table_name = 'admin';
$view_permission = 'public';
// Path for js and css files
$path = '../';
$username = '';
// -- HEADER
require_once($path . '../helper/db.php');
require_once($path . '../helper/functions.php');
session_start();

if (isset($_POST['submit'])) {
    // Escape strings
    $username = mysqli_escape_string($mysqli, $_POST["username"]);
    $password = $_POST["password"];

    // validations
    $required_fields = array('username', 'password');
    validate_presences($required_fields);

    $fields_with_max_lengths = array('username' => 30, 'password' => 32);
    validate_max_lengths($fields_with_max_lengths);

    if (!empty($errors)) {
        $_SESSION["errors"] = $errors;
    } else {
        $found_user = attempt_login($username, $password, 'admin');

        if ($found_user) {
            $_SESSION['adminUsername'] = $found_user['USERNAME'];
            $_SESSION['adminId'] = $found_user['id'];
            $_SESSION['adminAuth'] = 2;
            redirect_to($path . 'admin.php');
        } else {
            $_SESSION['message'] = 'Username or password do not match';
            $_SESSION['message_type'] = 'warning';
        }
    }
}
// Make sure path is set
if (!isset($path)) {
    $path = null;
}
if (!isset($view_permission)) {
    $view_permission = 'admin';
}
checkAccessPermissions($view_permission, $path);

?>
    <!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <!-- <meta http-equiv="X-UA-Compatible" content="IE=edge"> -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Online Wine Warehouse - A wide range of wines available online">
        <meta name="author" content="Group 30">
        <link rel="shortcut icon" href="<?php echo $path; ?>images/ico/favicon.ico">

        <title><?php echo $page_title; ?></title>

        <!-- Bootstrap core CSS -->
        <link href="<?php echo $path; ?>css/bootstrap.css" rel="stylesheet">
        <link href="<?php echo $path; ?>css/datepicker3.css" rel="stylesheet">
        <!-- Custom styles -->
        <link href="<?php echo $path; ?>css/carousel.css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script src="<?php echo $path; ?>js/bootstrap.min.js"></script>
        <script src="<?php echo $path; ?>js/docs.min.js"></script>
        <script src="<?php echo $path; ?>js/bootstrap-datepicker.js"></script>

    </head>

<body>
<!-- Content -->
<div class="container admin">
    <div class="row">
        <div class="col-sm-12">
            <?php echo form_errors(); ?>
            <h2 class="col-sm-offset-2"><?php echo $page_title; ?></h2>

            <form method="post" class="form-horizontal" action="">
                <div class="form-group">
                    <label for="username" class="required col-sm-2 control-label">Username<span
                            class="required">*</span></label>

                    <div class="col-xs-4">
                        <input type="text" class="form-control" id="username" name="username" placeholder="Username"
                               autocomplete="off" required="" autofocus=""
                               value="<?php echo htmlentities($username) ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="password" class="required col-sm-2 control-label">Password<span
                            class="required">*</span></label>

                    <div class="col-xs-4">
                        <input type="password" id="password" class="form-control" name="password" autocomplete="off"
                               placeholder="Password" required="">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" name="submit" value="submit" class="btn btn-default">Log In</button>
                    </div>
                </div>
            </form>
        </div>
        <!-- content -->


    </div>
    <!-- /.row -->


</div>
<!-- /.container -->

<?php
require_once($path . '../layout/footer.php');
