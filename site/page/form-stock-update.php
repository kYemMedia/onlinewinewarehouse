<?php
/**
 * Form Page
 *
 * Group 30
 * CI6230 Advanced Databases and Web
 * Kingston University
 */

// Page Title
$page_title = 'Update Wine Stock';
// Table name to get all information
$table_name = 'stock';
// Path for js and css files
$path = '../';
// -- HEADER
require_once($path . '../load.php');


if (isset($_GET['distribution_centre_id']) && isset($_GET['wine_id'])) {
    $d_id = (int)$_GET['distribution_centre_id'];
    $w_id = (int)$_GET['wine_id'];
    $stock = find_dist_stock($d_id, $w_id);
    $distribution_centre_id = $stock['DISTRIBUTION_CENTRE_ID'];
    $wine_id = $stock['WINE_ID'];
    $quantity = $stock['QUANTITY'];
    $date_available = $stock['DATE_AVAILABLE'];

    // If form was submited
    if (isset($_POST['submit'])) {

        // Escape strings, type cast int
        $quantity = (int)$_POST["quantity"];
        $date_available = date('Y-m-d', strtotime($_POST["date_available"]));

        // validations
        $required_fields = array('quantity');
        validate_presences($required_fields);

        $fields_with_max_lengths = array(
            'quantity' => 4
        );
        validate_max_lengths($fields_with_max_lengths);

        // Check if there is any errors
        if (!empty($errors)) {
            $_SESSION["errors"] = $errors;
        } else {
            // No errors, build query

            // Build query
            $query = "UPDATE `{$table_name}` ";
            $query .= "SET `quantity`= {$quantity},`date_available` = '{$date_available}' ";
            $query .= "WHERE `distribution_centre_id`={$d_id} ";
            $query .= "AND `wine_id`={$w_id} ";
            $query .= "Limit 1 ";

            saveInformation($query, $table_name);
        }
    }
}
//-- NAVBAR
require_once($path . '../layout/admin-navigation.php');
?>
    <!-- Content -->
    <div class="container admin">
        <div class="row">
            <div class="col-sm-12">
                <ol class="breadcrumb">
                    <li><a href="../site.php">Home</a></li>
                    <li class="active"><?php echo $page_title; ?></li>
                </ol>
                <?php echo form_errors(); ?>
                <h2 class="col-sm-offset-2"><?php echo $page_title; ?></h2>

                <div>
                    <form method="post" class="form-horizontal">
                        <div class="form-group">
                            <label for="sub_order" class="col-sm-2 control-label">Dist. Centre<span
                                    class="required">*</span></label>

                            <div class="col-xs-4">
                                <?php
                                $dist_centre_set = find_dist_centre_by_id($d_id);
                                $dist_centre_title = str_replace("_", " ", $dist_centre_set['NAME']);
                                ?>
                                <input class="form-control" id="sub_order" type="text"
                                       placeholder="<?php echo $dist_centre_title ?>" disabled>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="wine_title" class="col-sm-2 control-label">Wine<span
                                    class="required">*</span></label>

                            <div class="col-xs-4">
                                <?php
                                $wine_set = findById('wine', $w_id);
                                // Replace _ with space for brand name
                                $wine_title = str_replace("_", " ", $wine_set['name']);
                                ?>
                                <input class="form-control" id="wine_title" type="text"
                                       placeholder="<?php echo $wine_title ?>" disabled>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="quantity" class="required col-sm-2 control-label">Quantity </label>

                            <div class="col-xs-4">
                                <input maxlength="45" id="quantity" class="form-control" name="quantity" type="text"
                                       value="<?php echo $quantity ?>"></div>
                        </div>

                        <div class="form-group">
                            <label for="datepicker" class="col-sm-2 control-label">Available date</label>

                            <div class="col-xs-4">
                                <input class="datepicker" id="datepicker" name="date_available"
                                       value="<?php echo $date_available ?>">
                                <script type="text/javascript">$('.datepicker').datepicker()</script>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" name="submit" value="submit" class="btn btn-default">Update
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- content -->


        </div>
        <!-- /.row -->


        <!-- START THE FEATURETTES -->

        <hr class="featurette-divider">

        <div class="row featurette">
            <div class="col-md-7">
                <h2 class="featurette-heading">Bordeaux <span class="text-muted"> The great 2010 vintage produced some spectacular wines</span>
                </h2>

                <p class="lead">With over 10,000 properties, Bordeaux is a veritable treasure trove of fine and everyday
                    drinking. Styles range from modern to traditional, dry or sweet whites to easy-drinking and serious
                    cellar-worthy reds. We've sifted through the many bottles available to find members lovely wines
                    that punch above their weight</p>
            </div>
            <div class="col-md-5">
                <img class="featurette-image img-responsive" src="<?php echo $path; ?>images/wine-making.jpg"
                     alt="Generic placeholder image">
            </div>
        </div>

        <hr class="featurette-divider">
        <!-- /END THE FEATURETTES -->


        <!-- FOOTER -->
        <footer>
            <p class="pull-right"><a href="#">Back to top</a></p>

            <p>&copy; 2014 Online Wine Warehouse, Inc. &middot; <a href="#">Privacy</a> &middot; <a href="#">Terms</a>
            </p>
        </footer>

    </div><!-- /.container -->

<?php
require_once($path . '../layout/footer.php');