<?php

if (isset($_GET['clear']) && $_GET['clear'] == true) {
    $_SESSION['wines'] = null;
    $_SESSION['shopping_cart'] = null;
}
?>
<div class="order">
    <table class="table">
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Name</th>
            <th scope="col">Description</th>
            <th scope="col">Bottle Price</th>
            <th scope="col">Bottle Quantity</th>
            <th scope="col">Case Price</th>
            <th scope="col">Case Quantity</th>
            <th scope="col">Sub Total</th>
        </tr>
        <?php

        // If form was submitted
        if (isset($_POST['wine_order'])) {
            saveWineToSession();
        }

        // If there is any wine products in the session
        if (isset($_SESSION['wines'])) {
            // show it inside the table rows
            foreach ($_SESSION['wines'] as $wine_id => $wine_array) {
                // Cut the description, if it's too long
                $w_description = (strlen($wine_array['description']) > 120)
                    ? substr($wine_array['description'], 0, 117) . '...' : $wine_array['description'];
                ?>
                <tr>
                    <td><?php echo $wine_array['id']; ?></td>
                    <td><?php echo fieldNameAsText($wine_array['name']); ?></td>
                    <td><?php echo $w_description; ?></td>
                    <td><?php echo $wine_array['price']; ?></td>
                    <td><?php echo $wine_array['quantity']['bottle_quantity']; ?></td>
                    <td><?php echo $wine_array['case_price']; ?></td>
                    <td><?php echo $wine_array['quantity']['case_quantity']; ?></td>
                    <td><?php echo $wine_array['subtotal']; ?>
                    </td>
                </tr>
            <?php
            }
        }
        ?>
        <tr>
            <td colspan="7"><b>Shipping</b></td>
            <td><?php echo '£ ' . calculate_shipping(10); ?></td>
        </tr>
        <tr>
            <td colspan="7"><b>Tax</b></td>
            <td><?php echo '£ ' . add_tax(); ?></td>
        </tr>
        <tr>
            <td colspan="7"><b>Total</b></td>
            <td><?php echo '£ ' . calculate_order_total(); ?></td>
        </tr>
        <tr>
            <td colspan="8">
                <a href="page/place-order.php" class="btn btn-success btn-xs">
                    <span class="glyphicon glyphicon-tag"></span> Place order
                </a>
            </td>
        </tr>
    </table>
</div>

<a href="<?php echo $path; ?>site.php?subject=26" id="browse_wine">Browse Wines</a>
<a href="<?php echo $path; ?>site.php?subject=11&clear=true" id="aKill">
    <span class="glyphicon glyphicon-trash"></span>
    Clear Basket
</a>
        
      