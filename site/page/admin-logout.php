<?php
session_start();
if (isset($_SESSION['adminUsername']) && isset($_SESSION['adminAuth'])) {

    $_SESSION['adminUsername'] = null;
    $_SESSION['adminId'] = null;
    $_SESSION['adminAuth'] = null;
}

header('Location: ../admin.php');

