<?php
/**
 * Wishlist.php
 *
 * ### NOT IMPLEMENTED ###
 *
 *
 * Wish LIST for Online Wine Warehouse website
 *
 * Group 30
 * CI6230 Advanced Databases and Web
 * Kingston University
 */

$page_title = 'Online Wine Warehouse';
// Path for js and css files
$path = '../';
$view_permission = 'public';
// Current file name, required in navigation()
$file = 'site';

// -- HEADER
require_once($path . '../load.php');


//-- NAVBAR 
require_once($path . '../layout/navigation.php');

if (isset($_GET['clear']) && $_GET['clear'] == true) {
    $_SESSION['wines'] = null;
    $_SESSION['shopping_cart'] = null;
}
?>

    <!-- Content -->
    <div class="container admin">
        <div class="row">
            <div class="col-sm-9">
                <ol class="breadcrumb">
                    <li><a href="<?php echo $path; ?>index.php">Home</a></li>
                    <li class="active"><?php echo($page['title']); ?></li>
                </ol>
                <?php echo form_errors(); ?>
                <h2><?php echo($page['title']); ?></h2>
                <div class="order">
                    <table class="table">
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Name</th>
                            <th scope="col">Description</th>
                            <th scope="col">Bottle Price</th>
                            <th scope="col">Bottle Quantity</th>
                            <th scope="col">Case Price</th>
                            <th scope="col">Case Quantity</th>
                            <th scope="col">Sub Total</th>
                        </tr>
                        <?php
                        if (isset($_POST['wine_order'])) {

                            foreach ($_POST as $wine_id => $quantity_array) {

                                if (is_array($quantity_array) && ($quantity_array['bottle_quantity'] > 0 || $quantity_array['case_quantity'] > 0)) {
                                    $wine_set = find_all_data_id('wine', $wine_id);
                                    while ($wine = mysqli_fetch_assoc($wine_set)) {

                                        foreach ($wine as $key => $inner_value) {
                                            $_SESSION['wines'][$wine_id][$key] = $inner_value;
                                        }
                                        if ($wine_id == $wine_id) {
                                            $_SESSION['wines'][$wine_id]['quantity'] = $quantity_array;
                                        }
                                    }
                                }
                            } // End main for each
                        }
                        if (isset($_SESSION['wines'])) {
                        foreach ($_SESSION['wines'] as $wine_id => $wine_array){
                        ?>
                        <tr>
                            <td><?php echo $wine_array['id']; ?></td>
                            <td><?php echo fieldNameAsText($wine_array['name']); ?></td>
                            <td><?php echo $wine_array['description']; ?></td>
                            <td><?php echo $wine_array['price']; ?></td>
                            <td><?php echo $wine_array['quantity']['bottle_quantity']; ?></td>
                            <td><?php echo $wine_array['case_price']; ?></td>
                            <td><?php echo $wine_array['quantity']['case_quantity']; ?></td>
                            <td><?php echo '£ ' . $wine_array['subtotal']; ?></td>
                            <?php
                            }
                            }

                            ?>
                        <tr>
                            <td colspan="7"><b>Remove</b></td>
                            <td><?php echo ''; ?></td>
                        </tr>
                        <tr>
                            <td colspan="8"><a href="<?php echo $path ?>page/place-order.php" class="btn btn-success btn-xs"><span
                                        class="glyphicon glyphicon-tag"></span> Place order</a></td>
                        </tr>
                    </table>
                </div>

                <a href="site.php?subject=5&clear=true" id="aKill"><span class="glyphicon glyphicon-trash"></span> Clear
                    Basket</a>

            </div>
            <!-- /.col-sm-9 -->
            <!-- Sidebar -->
            <?php require_once($path . '../layout/sidebar.php'); ?>
        </div>
        <!-- /.row -->


        <!-- START THE FEATURETTES -->

        <hr class="featurette-divider">

        <div class="row featurette">
            <div class="col-md-7">
                <h2 class="featurette-heading">First featurette heading. <span
                        class="text-muted">It'll blow your mind.</span></h2>

                <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis
                    euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus,
                    tellus ac cursus commodo.</p>
            </div>
            <div class="col-md-5">
                <img class="featurette-image img-responsive" src="<?php echo $path; ?>images/botllegalss.jpg"
                     alt="Generic placeholder image">
            </div>
        </div>

        <hr class="featurette-divider">
        <!-- /END THE FEATURETTES -->


        <!-- FOOTER -->
        <footer>
            <p class="pull-right"><a href="#">Back to top</a></p>

            <p>&copy; 2014 Online Wine Warehouse, Inc. &middot; <a href="#">Privacy</a> &middot; <a href="#">Terms</a>
            </p>
        </footer>

    </div><!-- /.container -->

<?php
require_once($path . '../layout/footer.php');