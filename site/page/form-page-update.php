<?php
/**
 * Form Page
 *
 * Group 30
 * CI6230 Advanced Databases and Web
 * Kingston University
 */
$page_title = 'Update New Page';
$table_name = 'page';
// Path for js and css files
$path = '../';
// -- HEADER
require_once($path . '../load.php');

// Init values to avoid undefined notice
$subject_id = null;
$title = null;
$page_order = null;
$visible = null;
$content = null;
$position = null;
$file_name = null;

if (isset($_GET['id'])) {
    $id = mysqli_escape_string($mysqli, $_GET['id']);
    $page = findById($table_name, $id);
    $subject_id = $page['subject_id'];
    // $parent_subject = findById($table_name, $subject_id);
    $title = $page['title'];
    $page_order = $page['page_order'];
    $visible = $page['visible'];
    $content = $page['content'];
    $position = $page['position'];
    $file_name = $page["file_name"];

    if (isset($_POST['submit'])) {


        // Escape strings
        $subject_id = mysqli_escape_string($mysqli, $_POST["subject_id"]);
        $title = mysqli_escape_string($mysqli, $_POST["title"]);
        $page_order = (int)$_POST["page_order"];
        if (isset($_POST["visible"])) {
            $visible = (int)$_POST["visible"];
        } else {
            $visible = null;
        }
        $content = mysqli_escape_string($mysqli, $_POST["content"]);
        $position = mysqli_escape_string($mysqli, $_POST["position"]);
        $file_name = mysqli_escape_string($mysqli, $_POST["file_name"]);

        // validations
        $required_fields = array('title');
        validate_presences($required_fields);

        $fields_with_max_lengths = array('title' => 30, 'file_name' => 32);
        validate_max_lengths($fields_with_max_lengths);

        // Check if there is any errors
        if (!empty($errors)) {
            $_SESSION["errors"] = $errors;
        } else {

            // Build query
            $query = "UPDATE `{$table_name}` ";
            $query .= "SET`subject_id`={$subject_id}, `title`='{$title}', ";
            $query .= "`page_order`={$page_order}, `visible`={$visible}, `content`='{$content}', ";
            $query .= "`position`='{$position}', `file_name`='{$file_name}' ";
            $query .= "WHERE id={$id} ";
            $query .= "Limit 1 ";
            echo($query);
            saveInformation($query, $table_name);

            // Alternative Way -> Bind Param
            // $stmt = $mysqli->prepare("UPDATE page (f1, f2) VALUES (?, ?)");
            // if ($stmt === false) {
            //   trigger_error($this->mysqli->error, E_USER_ERROR);
            // }
            // $stmt->bind_param('ss', $field1, $field2);

            // $field1 = "String Value";
            // $field2 = null;
        }
    }
}
//-- NAVBAR
require_once($path . '../layout/admin-navigation.php');
?>

    <!-- Content -->
    <div class="container admin">
        <div class="row">
            <div class="col-sm-12">
                <?php echo form_errors(); ?>
                <ol class="breadcrumb">
                    <li><a href="../site.php">Home</a></li>
                    <li class="active"><?php echo $page_title; ?></li>
                </ol>
                <h2 class="col-sm-offset-2"><?php echo $page_title; ?></h2>

                <div>
                    <div>
                        <form method="post" class="form-horizontal">

                            <div class="form-group">
                                <label for="title" class="required col-sm-2 control-label">Title <span class="required">*</span></label>

                                <div class="col-xs-4">
                                    <input maxlength="45" id="title" class="form-control" name="title" type="text"
                                           value="<?php echo $title ?>"></div>
                            </div>
                            <div class="form-group">
                                <label for="content" class="required col-sm-2 control-label">Content <span
                                        class="required">*</span></label>

                                <div class="col-xs-10">
                                    <textarea id="content" class="form-control" name="content"
                                              rows="20"><?php echo $content ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="position" class="required col-sm-2 control-label">Position <span
                                        class="required">*</span></label>

                                <div class="col-xs-4">
                                    <input maxlength="45" id="position" class="form-control" name="position" type="text"
                                           value="<?php echo $position ?>"></div>
                            </div>
                            <div class="form-group">
                                <label for="parent_subject" class="col-sm-2 control-label">Parent Subject<span
                                        class="required">*</span></label>

                                <div class="col-xs-4">
                                    <select id="parent_subject" class="form-control" name="subject_id">
                                        <?php
                                        $result = findAll('subjects');

                                        echo "<option value=\"NULL\">No Parent Subject</option>";

                                        while ($subject = $result->fetch_assoc()) {

                                            echo "<option value=\"{$subject['id']}\" ";
                                            if ($subject['id'] == $subject_id) {
                                                echo 'selected';
                                            }


                                            echo ">{$subject['title']}</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="file_name" class="required col-sm-2 control-label">File Name<span
                                        class="required">*</span></label>

                                <div class="col-xs-4">
                                    <input maxlength="45" id="file_name" class="form-control" name="file_name"
                                           type="text" value="<?php echo $file_name ?>"></div>
                            </div>

                            <div class="form-group">
                                <label for="page_order" class="col-sm-2 control-label">Page Order</label>

                                <div class="col-xs-4">

                                    <select id="page_order" class="form-control" name="page_order">
                                        <?php
                                        $page_count = mysqli_num_rows($result);
                                        echo "<option value=\"\">Please Select</option>";
                                        for ($i = 1; $i <= ($page_count + 1); $i ++) {

                                            echo "<option value='{$i}'";
                                            if ($page_order == $i) {
                                                echo 'selected';
                                            }
                                            echo ">{$i}</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <div class="checkbox">
                                        <label for="visible">
                                            <input name="visible" id="visible" type="checkbox" value="1"
                                                <?php if ($visible == 1) {
                                                    echo "checked";
                                                }
                                                ?>>Visible
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" name="submit" value="submit" class="btn btn-default">Update
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- content -->


        </div>
        <!-- /.row -->


        <!-- START THE FEATURETTES -->

        <hr class="featurette-divider">

        <div class="row featurette">
            <div class="col-md-7">
                <h2 class="featurette-heading">Bordeaux <span class="text-muted"> The great 2010 vintage produced some spectacular wines</span>
                </h2>

                <p class="lead">With over 10,000 properties, Bordeaux is a veritable treasure trove of fine and everyday
                    drinking. Styles range from modern to traditional, dry or sweet whites to easy-drinking and serious
                    cellar-worthy reds. We've sifted through the many bottles available to find members lovely wines
                    that punch above their weight</p>
            </div>
            <div class="col-md-5">
                <img class="featurette-image img-responsive" src="<?php echo $path; ?>images/wine-making.jpg"
                     alt="Generic placeholder image">
            </div>
        </div>

        <hr class="featurette-divider">
        <!-- /END THE FEATURETTES -->


        <!-- FOOTER -->
        <footer>
            <p class="pull-right"><a href="#">Back to top</a></p>

            <p>&copy; 2014 Online Wine Warehouse, Inc. &middot; <a href="#">Privacy</a> &middot; <a href="#">Terms</a>
            </p>
        </footer>

    </div><!-- /.container -->

<?php
require_once($path . '../layout/footer.php');