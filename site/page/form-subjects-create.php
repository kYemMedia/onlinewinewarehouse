<?php
/**
 * Form Page
 *
 * Group 30
 * CI6230 Advanced Databases and Web
 * Kingston University
 */
$page_title = 'Create New Subject';
$table_name = 'subjects';
// Path for js and css files
$path = '../';
// -- HEADER
require_once($path . '../load.php');

// Init values to avoid undefined notice
$title = null;
$position = null;
$file_name = null;
$sub_order = null;
$visible = null;

if (isset($_POST['submit'])) {


    // Escape strings
    $title = mysqli_escape_string($mysqli, $_POST["title"]);
    $sub_order = (int)$_POST["sub_order"];
    if (isset($_POST["visible"])) {
        $visible = (int)$_POST["visible"];
    } else {
        $visible = null;
    }
    $position = mysqli_escape_string($mysqli, $_POST["position"]);
    $file_name = mysqli_escape_string($mysqli, $_POST["file_name"]);

    // validations
    $required_fields = array('title');
    validate_presences($required_fields);

    $fields_with_max_lengths = array('title' => 30, 'file_name' => 32);
    validate_max_lengths($fields_with_max_lengths);

    // Check if there is any errors
    if (!empty($errors)) {
        $_SESSION["errors"] = $errors;
    } else {

        // Build query
        $query = "INSERT INTO `subjects` ";
        $query .= "(`title`, `sub_order`, `visible`,`position`, `file_name`) ";
        $query .= "VALUES ";
        $query .= "('{$title}',{$sub_order},{$visible}, '{$position}', '{$file_name}' )";

        saveInformation($query, $table_name);
    }
}
//-- NAVBAR
require_once($path . '../layout/admin-navigation.php');
?>

    <!-- Content -->
    <div class="container admin">
        <div class="row">
            <div class="col-sm-12">
                <?php echo form_errors(); ?>
                <ol class="breadcrumb">
                    <li><a href="../site.php">Home</a></li>
                    <li class="active"><?php echo $page_title; ?></li>
                </ol>
                <h2 class="col-sm-offset-2"><?php echo $page_title; ?></h2>

                <div>
                    <div>
                        <form method="post" class="form-horizontal">
                            <div class="form-group">
                                <label for="title" class="required col-sm-2 control-label">Title <span class="required">*</span></label>

                                <div class="col-xs-4">
                                    <input maxlength="45" id="title" class="form-control" name="title" type="text"
                                           value="<?php echo $title ?>"></div>
                            </div>
                            <div class="form-group">
                                <label for="position" class="required col-sm-2 control-label">Position <span
                                        class="required">*</span></label>

                                <div class="col-xs-4">
                                    <input maxlength="45" id="position" class="form-control" name="position" type="text"
                                           value="<?php echo $position ?>"></div>
                            </div>
                            <div class="form-group">
                                <label for="file_name" class="required col-sm-2 control-label">File Name<span
                                        class="required">*</span></label>

                                <div class="col-xs-4">
                                    <input maxlength="45" id="file_name" class="form-control" name="file_name"
                                           type="text" value="<?php echo $file_name ?>"></div>
                            </div>
                            <div class="form-group">
                                <label for="sub_order" class="col-sm-2 control-label">Order</label>

                                <div class="col-xs-4">

                                    <select id="sub_order" class="form-control" name="sub_order">
                                        <?php
                                        $subject_set = find_all_subjects();
                                        $subject_count = mysqli_num_rows($subject_set);
                                        echo "<option value=\"\">Please Select</option>";
                                        for ($i = 1; $i <= ($subject_count + 1); $i ++) {

                                            echo "<option value='{$i}'";
                                            if ($sub_order == $i) {
                                                echo 'selected';
                                            }
                                            echo ">{$i}</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <div class="checkbox">
                                        <label for="visible">
                                            <input name="visible" id="visible" type="checkbox" value="1"
                                                <?php if ($visible == 1) {
                                                    echo "checked";
                                                }
                                                ?>>Visible
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" name="submit" value="submit" class="btn btn-default">Create
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- content -->


        </div>
        <!-- /.row -->


        <!-- START THE FEATURETTES -->

        <hr class="featurette-divider">

        <div class="row featurette">
            <div class="col-md-7">
                <h2 class="featurette-heading">Bordeaux <span class="text-muted"> The great 2010 vintage produced some spectacular wines</span>
                </h2>

                <p class="lead">With over 10,000 properties, Bordeaux is a veritable treasure trove of fine and everyday
                    drinking. Styles range from modern to traditional, dry or sweet whites to easy-drinking and serious
                    cellar-worthy reds. We've sifted through the many bottles available to find members lovely wines
                    that punch above their weight</p>
            </div>
            <div class="col-md-5">
                <img class="featurette-image img-responsive" src="<?php echo $path; ?>images/wine-making.jpg"
                     alt="Generic placeholder image">
            </div>
        </div>

        <hr class="featurette-divider">
        <!-- /END THE FEATURETTES -->


        <!-- FOOTER -->
        <footer>
            <p class="pull-right"><a href="#">Back to top</a></p>

            <p>&copy; 2014 Online Wine Warehouse, Inc. &middot; <a href="#">Privacy</a> &middot; <a href="#">Terms</a>
            </p>
        </footer>

    </div><!-- /.container -->

<?php
require_once($path . '../layout/footer.php');