<?php
/**
 * Form Page
 *
 * Group 30
 * CI6230 Advanced Databases and Web
 * Kingston University
 */
$page_title = 'Create Admin';

// Path for js and css files
$path = '../';
$table_name = 'admin';
// -- HEADER
require_once($path . '../load.php');

// Init values to avoid undefined notice
$username = null;
$password = null;


if (isset($_POST['submit'])) {

    // Escape strings
    $username = mysqli_escape_string($mysqli, $_POST["username"]);
    $password = password_encrypt($_POST["password"]);

    // validations
    $required_fields = array('username', 'password');
    validate_presences($required_fields);

    $fields_with_max_lengths = array('username' => 30, 'password' => 32);
    validate_max_lengths($fields_with_max_lengths);

    // Check if there is any errors
    if (!empty($errors)) {
        $_SESSION["errors"] = $errors;
    } else {

        // Build query
        $query = "INSERT INTO `admin` ";
        $query .= "(`username`, `password`) ";
        $query .= "VALUES ";
        $query .= "('{$username}','{$password}' )";

        saveInformation($query, $table_name);
    }
}
//-- NAVBAR
require_once($path . '../layout/admin-navigation.php');
?>

    <!-- Content -->
    <div class="container admin">
        <div class="row">
            <div class="col-sm-12">
                <?php echo form_errors(); ?>
                <ol class="breadcrumb">
                    <li><a href="../site.php">Home</a></li>
                    <li class="active"><?php echo $page_title; ?></li>
                </ol>
                <h2 class="col-sm-offset-2"><?php echo $page_title; ?></h2>

                <div>
                    <div>
                        <form method="post" class="form-horizontal">
                            <div class="form-group">
                                <label for="username" class="required col-sm-2 control-label">Username <span
                                        class="required">*</span></label>

                                <div class="col-xs-4">
                                    <input maxlength="45" id="username" class="form-control" name="username" type="text"
                                           autocomplete="off" value="<?php echo $username ?>"></div>
                            </div>
                            <div class="form-group">
                                <label for="password" class="required col-sm-2 control-label">Password<span
                                        class="required">*</span></label>

                                <div class="col-xs-4">
                                    <input maxlength="45" id="password" class="form-control" name="password"
                                           type="password" autocomplete="off" value="<?php echo $password ?>"></div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" name="submit" value="submit" class="btn btn-default">Create
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- content -->


        </div>
        <!-- /.row -->

        <!-- START THE FEATURETTES -->

        <hr class="featurette-divider">

        <div class="row featurette">
            <div class="col-md-7">
                <h2 class="featurette-heading">Bordeaux <span class="text-muted"> The great 2010 vintage produced some spectacular wines</span>
                </h2>

                <p class="lead">With over 10,000 properties, Bordeaux is a veritable treasure trove of fine and everyday
                    drinking. Styles range from modern to traditional, dry or sweet whites to easy-drinking and serious
                    cellar-worthy reds. We've sifted through the many bottles available to find members lovely wines
                    that punch above their weight</p>
            </div>
            <div class="col-md-5">
                <img class="featurette-image img-responsive" src="<?php echo $path; ?>images/wine-making.jpg"
                     alt="Generic placeholder image">
            </div>
        </div>

        <hr class="featurette-divider">
        <!-- /END THE FEATURETTES -->


        <!-- FOOTER -->
        <footer>
            <p class="pull-right"><a href="#">Back to top</a></p>

            <p>&copy; 2014 Online Wine Warehouse, Inc. &middot; <a href="#">Privacy</a> &middot; <a href="#">Terms</a>
            </p>
        </footer>

    </div><!-- /.container -->

<?php
require_once($path . '../layout/footer.php');