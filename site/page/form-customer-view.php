<?php
/**
 * Form Page
 *
 * Group 30
 * CI6230 Advanced Databases and Web
 * Kingston University
 */

// Path for js and css files
$path = '../';
// Table name to get all information outputed in a table
$table_name = 'customer';
// -- HEADER
$page_title = 'Manage Subjects';
require_once($path . '../load.php');

//-- NAVBAR
require_once($path . '../layout/admin-navigation.php');
?>

    <!-- Content -->
    <div class="container admin">
        <div class="row">
            <div class="col-sm-12">
                <?php echo form_errors(); ?>
                <ol class="breadcrumb">
                    <li><a href="../site.php">Home</a></li>
                    <li class="active"><?php echo $page_title; ?></li>
                </ol>
                <h2 class="col-sm-offset-2">Manage <?php echo fieldNameAsText($table_name); ?></h2>

                <div>
                    <div>
                        <table class="table">
                            <thead>
                            <tr>
                                <?php show_table_column_names($table_name); ?>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $result = findAll($table_name);
                            if ($result) {
                                while ($row = $result->fetch_array()) {
                                    show_table_column_data($table_name, $row);
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                        <!-- Modal -->

                    </div>
                </div>
            </div>
            <!-- content -->


        </div>
        <!-- /.row -->

        <!-- FOOTER -->
        <footer>
            <p class="pull-right"><a href="#">Back to top</a></p>

            <p>&copy; 2014 Online Wine Warehouse, Inc. &middot; <a href="#">Privacy</a> &middot; <a href="#">Terms</a>
            </p>
        </footer>

    </div><!-- /.container -->

<?php
require_once($path . '../layout/footer.php');