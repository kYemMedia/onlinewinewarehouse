<?php
/**
 * Form Page
 *
 * Group 30
 * CI6230 Advanced Databases and Web
 * Kingston University
 */
$page_title = 'Update Wine';
$table_name = 'wine';
// Path for js and css files
$path = '../';
// -- HEADER
require_once($path . '../load.php');

// Init values to avoid undefined notice
$result = get_table_column_names($table_name);
while ($column = $result->fetch_array()) {
    // $index = $column[0];
    $$column[0] = null;
    $string = 'title_' . $column[0];
    $$string = $column[0];
    echo $$column[0];
}

if (isset($_GET['id'])) {
    $id = (int)$_GET['id'];
    $subject = findById($table_name, $id);
    $name = $subject['name'];
    $colour = $subject['colour'];
    $description = $subject['description'];
    $indicator = $subject['indicator'];
    $style = $subject['style'];
    $type = $subject['type'];
    $size = $subject['size'];
    $bottle_per_case = $subject['bottle_per_case'];
    $price = $subject['price'];
    $case_price = $subject['case_price'];
    $available = $subject['available'];
    $country = $subject['country'];


    if (isset($_POST['submit'])) {


        // Escape strings
        $id_post = (int)$_POST["id"];
        $name = mysqli_escape_string($mysqli, $_POST["name"]);
        $colour = mysqli_escape_string($mysqli, $_POST["colour"]);
        $description = mysqli_escape_string($mysqli, $_POST["description"]);
        $indicator = (int)$_POST["indicator"];
        $style = mysqli_escape_string($mysqli, $_POST["style"]);
        $type = mysqli_escape_string($mysqli, $_POST["type"]);
        $size = mysqli_escape_string($mysqli, $_POST["size"]);
        $bottle_per_case = (int)$_POST["bottle_per_case"];
        $price = (float)$_POST["price"];
        $case_price = (float)$_POST["case_price"];
        if (isset($_POST["available"])) {
            $available = (int)$_POST["available"];
        } else {
            $available = 0;
        }
        $country = mysqli_escape_string($mysqli, $_POST["country"]);

        // validations
        $required_fields = array(
            'id', 'name', 'colour', 'description', 'type', 'size',
            'bottle_per_case', 'price', 'case_price', 'country'
        );
        validate_presences($required_fields);

        $fields_with_max_lengths = array(
            'id'              => 8,
            'name'            => 50,
            'colour'          => 50,
            'description'     => 500,
            'indicator'       => 2,
            'style'           => 11,
            'type'            => 10,
            'size'            => 11,
            'bottle_per_case' => 2,
            'price'           => 5,
            // +1 for . sign
            'case_price'      => 6,
            'country'         => 35,

        );
        validate_max_lengths($fields_with_max_lengths);

        // Check if there is any errors
        if (!empty($errors)) {
            $_SESSION["errors"] = $errors;
        } else {

            // Build query
            $query = "UPDATE {$table_name} ";
            $query .= "SET `id`={$id_post}, `name`='{$name}', ";
            $query .= "`colour`='{$colour}',`description`='{$description}', ";
            $query .= "`indicator`={$indicator},`style`='{$style}', ";
            $query .= "`type`='{$type}',`size`='{$size}', ";
            $query .= "`bottle_per_case`={$bottle_per_case},`price`={$price}, ";
            $query .= "`case_price`={$case_price}, `available`={$available}, `country`='{$country}'";
            $query .= "WHERE id={$id}";
            // echo $query;

            saveInformation($query, $table_name);
        }
    }
}
//-- NAVBAR
require_once($path . '../layout/admin-navigation.php');
?>

    <!-- Content -->
    <div class="container admin">
        <div class="row">
            <div class="col-sm-12">
                <?php echo form_errors(); ?>
                <ol class="breadcrumb">
                    <li><a href="../site.php">Home</a></li>
                    <li class="active"><?php echo $page_title; ?></li>
                </ol>
                <h2 class="col-sm-offset-2"><?php echo $page_title; ?></h2>

                <div>
                    <div>
                        <form method="post" class="form-horizontal">
                            <div class="form-group">
                                <label for="id"
                                       class="required col-sm-2 control-label"><?php echo fieldNameAsText($title_id) ?>
                                    <span class="required">*</span></label>

                                <div class="col-xs-4">
                                    <input maxlength="45" id="id" class="form-control" name="id" type="text"
                                           value="<?php echo $id ?>"></div>
                            </div>
                            <div class="form-group">
                                <label for="name"
                                       class="required col-sm-2 control-label"><?php echo fieldNameAsText($title_name) ?>
                                    <span class="required">*</span></label>

                                <div class="col-xs-4">
                                    <input maxlength="45" id="name" class="form-control" name="name" type="text"
                                           value="<?php echo $name ?>"></div>
                            </div>
                            <div class="form-group">
                                <label for="colour"
                                       class="required col-sm-2 control-label"><?php echo fieldNameAsText($title_colour) ?>
                                    <span class="required">*</span></label>

                                <div class="col-xs-4">
                                    <input maxlength="45" id="colour" class="form-control" name="colour" type="text"
                                           value="<?php echo $colour ?>"></div>
                            </div>
                            <div class="form-group">
                                <label for="description"
                                       class="required col-sm-2 control-label"><?php echo fieldNameAsText($title_description) ?>
                                    <span class="required">*</span></label>

                                <div class="col-xs-4">
                    <textarea class="form-control" id="description" class="form-control" name="description" rows="7">
                     <?php echo $description ?>
                    </textarea></div>
                            </div>
                            <div class="form-group">
                                <label for="indicator"
                                       class="required col-sm-2 control-label"><?php echo fieldNameAsText($title_indicator) ?></label>

                                <div class="col-xs-4">
                                    <input maxlength="45" id="indicator" class="form-control" name="indicator"
                                           type="text" value="<?php echo $indicator ?>"></div>
                            </div>
                            <div class="form-group">
                                <label for="style"
                                       class="required col-sm-2 control-label"><?php echo fieldNameAsText($title_style) ?></label>

                                <div class="col-xs-4">
                                    <input maxlength="45" id="style" class="form-control" name="style" type="text"
                                           value="<?php echo $style ?>"></div>
                            </div>
                            <div class="form-group">
                                <label for="type"
                                       class="required col-sm-2 control-label"><?php echo fieldNameAsText($title_type) ?>
                                    <span class="required">*</span></label>

                                <div class="col-xs-4">
                                    <input maxlength="45" id="type" class="form-control" name="type" type="text"
                                           value="<?php echo $type ?>"></div>
                            </div>
                            <div class="form-group">
                                <label for="size"
                                       class="required col-sm-2 control-label"><?php echo fieldNameAsText($title_size) ?>
                                    <span class="required">*</span></label>

                                <div class="col-xs-4">
                                    <input maxlength="45" id="size" class="form-control" name="size" type="text"
                                           value="<?php echo $size ?>"></div>
                            </div>
                            <div class="form-group">
                                <label for="bottle_per_case"
                                       class="required col-sm-2 control-label"><?php echo fieldNameAsText($title_bottle_per_case) ?>
                                    <span class="required">*</span></label>

                                <div class="col-xs-4">
                                    <input maxlength="45" id="bottle_per_case" class="form-control"
                                           name="bottle_per_case" type="text" value="<?php echo $bottle_per_case ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="price"
                                       class="required col-sm-2 control-label"><?php echo fieldNameAsText($title_price) ?>
                                    <span class="required">*</span></label>

                                <div class="col-xs-4">
                                    <input maxlength="45" id="price" class="form-control" name="price" type="text"
                                           value="<?php echo $price ?>"></div>
                            </div>
                            <div class="form-group">
                                <label for="case_price"
                                       class="required col-sm-2 control-label"><?php echo fieldNameAsText($title_case_price) ?>
                                    <span class="required">*</span></label>

                                <div class="col-xs-4">
                                    <input maxlength="45" id="case_price" class="form-control" name="case_price"
                                           type="text" value="<?php echo $case_price ?>"></div>
                            </div>


                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <div class="checkbox">
                                        <label for="available">
                                            <input name="available" id="available" type="checkbox" value="1"
                                                <?php if ($available == 1) {
                                                    echo "checked";
                                                }
                                                ?>><?php echo fieldNameAsText($title_available); ?>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="country"
                                       class="required col-sm-2 control-label"><?php echo fieldNameAsText($title_country) ?>
                                    <span class="required">*</span></label>

                                <div class="col-xs-4">
                                    <input maxlength="45" id="country" class="form-control" name="country" type="text"
                                           value="<?php echo $country ?>"></div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" name="submit" value="submit" class="btn btn-default">Update
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- content -->


        </div>
        <!-- /.row -->


        <!-- START THE FEATURETTES -->

        <hr class="featurette-divider">

        <div class="row featurette">
            <div class="col-md-7">
                <h2 class="featurette-heading">Bordeaux <span class="text-muted"> The great 2010 vintage produced some spectacular wines</span>
                </h2>

                <p class="lead">With over 10,000 properties, Bordeaux is a veritable treasure trove of fine and everyday
                    drinking. Styles range from modern to traditional, dry or sweet whites to easy-drinking and serious
                    cellar-worthy reds. We've sifted through the many bottles available to find members lovely wines
                    that punch above their weight</p>
            </div>
            <div class="col-md-5">
                <img class="featurette-image img-responsive" src="<?php echo $path; ?>images/wine-making.jpg"
                     alt="Generic placeholder image">
            </div>
        </div>

        <hr class="featurette-divider">
        <!-- /END THE FEATURETTES -->


        <!-- FOOTER -->
        <footer>
            <p class="pull-right"><a href="#">Back to top</a></p>

            <p>&copy; 2014 Online Wine Warehouse, Inc. &middot; <a href="#">Privacy</a> &middot; <a href="#">Terms</a>
            </p>
        </footer>

    </div><!-- /.container -->

<?php
require_once($path . '../layout/footer.php');