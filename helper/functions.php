<?php
/**
 * CI6230 Advanced Databases and Web
 * Group 30, k1037813
 * Kingston University
 *
 */

/**
 * Only for development testing
 * @param array $data
 */
function kill($data)
{
    var_dump($data);
    die();
}

/**
 * Shorter version of redirect
 * @param string $path
 * @return void
 */
function redirect_to($path)
{
    header('Location: ' . $path);
    // Make Sure nothing else happens
    exit;
}

/**
 * Confirm that query was executed successfully
 * other wise throw an error and die
 *
 * @NOTE should try to catch the error instead
 *
 * @param MySQL Object $result_set
 * @return void
 */
function confirm_query($result_set)
{
    global $mysqli;
    if (!$result_set) {
        $_SESSION['message'] = "Error. SQL Query failed: (" . $mysqli->errno . ") " . $mysqli->error;
        $_SESSION['message_type'] = 'danger';
    }
}


/**
 * Get the current page url
 * @return String url
 */
function current_url()
{
    $pageURL = 'http';
    if ($_SERVER["HTTPS"] == "on") {
        $pageURL .= "s";
    }
    $pageURL .= "://";
    if ($_SERVER["SERVER_PORT"] != "80") {
        $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
    } else {
        $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
    }

    return $pageURL;
}


/*============================================================================
 *
 *      Functions for getting information about table columns
 */

/**
 * Get column names of specified table
 *
 * @TODO Refactor to allow select specific columns
 *       If not provided, default = '*' (select all)
 *
 * @param string $table_name
 * @return mysqli_result $result
 */
function get_table_column_names($table_name)
{
    // Need to define database name as well in case there are
    // multiple ones with same column names
    $query = "SELECT `COLUMN_NAME` ";
    $query .= "FROM `INFORMATION_SCHEMA`.`COLUMNS` ";
    $query .= "WHERE `TABLE_SCHEMA`='" . DB_NAME . "' ";
    $query .= "AND `TABLE_NAME`='{$table_name}' ";
    $result = executeQuery($query);
    confirm_query($result);

    return $result;
}

/**
 * Gets name, data type(max_length) from db
 * Can be used in validation based on database restrcitions
 *
 * @TODO Automate validation, CHARACTER_MAXIMUM_LENGTH strings only
 *       as integers, returns 0. Need alternative solution.
 *
 * @param string $table_name
 * @return array $result
 */
function get_table_column_type_lenght($table_name)
{
    $query = "SELECT `COLUMN_NAME`, `DATA_TYPE`, `CHARACTER_MAXIMUM_LENGTH` ";
    $query .= "FROM `INFORMATION_SCHEMA`.`COLUMNS` ";
    $query .= "WHERE `TABLE_SCHEMA`='" . DB_NAME . "' ";
    $query .= "AND `TABLE_NAME`='{$table_name}' ";
    $result = executeQuery($query);
    confirm_query($result);

    return $result;
}

/**
 * Populate table with data from database table
 * First column in the database table must be id
 * @param string $table_name
 * @param array $current_row
 * @return string $output
 */
function show_table_column_data($table_name, $current_row)
{
    $result = get_table_column_names($table_name);
    $id_column = null;
    $output = '<tr>';
    while ($column = mysqli_fetch_array($result)) {
        $index = $column[0];
        $output .= '<td>' . htmlentities($current_row[$index]) . '</td>';
        if (!isset($id_column)) {
            $id_column = $column[0];
        };

    }

    // table first column must be ID
    $output .= '<td>';
    // Edit
    $output .= '<a href="form-' . $table_name . '-update.php?id=' . $current_row[0] . '">';
    $output .= '<span class="glyphicon glyphicon-pencil"></span></a>';
    // Delete
    $output .= '<a data-toggle="modal" data-target="#delete_modal_' . $current_row[0] . '" class="btn  btn-xs">';
    $output .= '<span class="glyphicon glyphicon-trash"></span></a>';
    $output .= create_delete_confirmation_modal($table_name, $id_column, $current_row[0]);
    $output .= '</td></tr>';

    echo $output;
}

/**
 * Populate table with data from database table
 * First column in the database table must be id
 * @param string $table_name
 * @param mixed $current_row
 * @return void
 */
function show_stock_column_data($table_name, $current_row)
{
    $second_column_name = 'WINE_ID';
    $result = get_table_column_names($table_name);

    $id_column = null;
    $id_column_2 = null;
    $output = '<tr>';
    while ($column = mysqli_fetch_array($result)) {
        $index = $column[0];
        $output .= '<td>' . htmlentities($current_row[$index]) . '</td>';
        if (!isset($id_column) && !isset($id_column_2)) {
            $id_column = $column[0];
        };

    }
    // table first column must be ID
    $output .= '<td>';
    // Edit
    $output .= '<a href="form-' . $table_name . '-update.php?distribution_centre_id=' . $current_row[0] . '&wine_id=' . $current_row[1] . '">';
    $output .= '<span class="glyphicon glyphicon-pencil"></span></a>';
    // Delete
    $output .= '<a data-toggle="modal" data-target="#delete_modal_' . $current_row[0] . '" class="btn  btn-xs">';
    $output .= '<span class="glyphicon glyphicon-trash"></span></a>';
    // Requires table_namem, column
    $output .= create_delete_confirmation_modal($table_name, $id_column, $current_row[0], $second_column_name, $current_row[1]);
    $output .= '</td></tr>';

    echo $output;
}

/**
 * Creates Modal popup for delete confirmation
 *
 * @param string $table_name
 * @param string $id_column
 * @param int $id
 * @param boolean|string $id_column_2
 * @param boolean|int $id_2
 * @return string $output - Whole Delete Confirmation Modal
 */
function create_delete_confirmation_modal($table_name, $id_column, $id, $id_column_2 = false, $id_2 = false)
{
    $output = ' <div class="modal fade" id="delete_modal_' . $id . '" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Delete Subject</h4>
              </div>
              <div class="modal-body">
                Are you sure?
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <form method="POST" action="delete.php" accept-charset="UTF-8" style="display:inline">
                    <input type="hidden" name="column_name" value="' . $id_column . '">
                    <input type="hidden" name="id" value="' . $id . '">';
    if ($id_column_2) {
        $output .= '<input type="hidden" name="column_name_2" value="' . $id_column_2 . '">';
        $output .= '<input type="hidden" name="id_2" value="' . $id_2 . '">';
    }
    $output .= '<input type="hidden" name="table" value="' . $table_name . '">
                    <button class="btn btn-xs btn-danger" type="submit" data-toggle="modal" name="submit" data-target="#delete_modal" data-title="Delete User" data-message="Are you sure you want to delete this user ?">
                        <i class="glyphicon glyphicon-trash"></i> Delete
                    </button>
                </form>
              </div>
            </div>
          </div>
        </div>';

    return $output;
}

/**
 * Show database table column names
 * @param  string $table_name
 * @return echo the table headers in <th> tags
 */
function show_table_column_names($table_name)
{

    $result = get_table_column_names($table_name);
    $output = '';
    while ($header = $result->fetch_assoc()) {
        $output .= '<th>' . fieldNameAsText($header["COLUMN_NAME"]) . '</th>';
    }

    echo $output;
}

/**
 * Generate a wine list from the database
 *
 * @param  boolean $category
 * @return string $output
 */
function wine_list($category = null)
{
    $checkoutButton  = '<div><input class="btn btn-success btn-xs" type="submit" name="wine_order" value="Add to Basket" /></div>';

    $output = '<div class="wine-list">';
    $output .= '<form action="site.php?subject=11" method="post" class="basket">';
    $output .= $checkoutButton;

    $wine_set = find_all_wines($category);

    while ($result = mysqli_fetch_assoc($wine_set)) {

        $wine = sanitizeHtmlOutput($result);
        // Make sure its not too big
        $w_description = (strlen($wine["description"]) > 80) ? substr($wine["description"], 0, 77) . '...' : $wine["description"];

        $output .= '<ul><li id ="' . $wine["id"] . '">';
        $output .= '<h4>' . $wine['name'] . '</h4></li>';
        $output .= '<li class="wine-img"><img src="images/' . fieldNameAsText($wine['name'], false) . '.jpg" alt="' . $wine['name'] . '"></li></li>';
        $output .= '<li class="description"> ' . $w_description . '</li>';
        $output .= '<li>Wine Type:  ' . $wine["type"] . '</li>';
        $output .= '<li>Wine Colour: ' . $wine["colour"] . '</li>';
        $output .= '</li><li class="price_field">Bottle price £ ' . $wine['price'] . '</li>';
        $output  = wineListBottleQualitySelect($wine['id'], $output);
        $output .= '<li>Available: ' . $wine["bottle_per_case"] . ' bottle case </li>';
        $output .= '<li class="price_field">Case price £ ' . $wine["case_price"] . '</li>';
        $output  = wineListCaseQualitySelect($wine['id'], $output);
        $output .= '</ul>';
    }
    $output .= $checkoutButton;
    $output .= '</form></div>';
    mysqli_free_result($wine_set);

    return $output;
}

/**
 * @param $wine
 * @return string
 */
function sanitizeHtmlOutput($wine)
{
        $result = array();
        foreach ($wine as $key => $wineProperty) {
            $result[$key] = htmlentities($wineProperty);
        }
        return $result;
}

/**
 * @param $w_id
 * @param $output
 * @return string
 */
function wineListBottleQualitySelect($w_id, $output)
{
    $output .= '<li>';
        $output .= '<select class="form-control" name="' . $w_id . '[bottle_quantity]" id=btl-qty-"' . $w_id . '" />';
        $output .= '<option selected value="0">Select Quantity</option>';
        $output .= '<option value="1">1</option>';
        $output .= '<option value="2">2</option>';
        $output .= '<option value="3">3</option>';
        $output .= '<option value="4">4</option>';
        $output .= '<option value="5">5</option>';
        $output .= '<option value="6">6</option>';
        $output .= '<option value="7">7</option>';
        $output .= '<option value="8">8</option>';
        $output .= '<option value="9">9</option>';
        $output .= '<option value="10">10</option>';
        $output .= '<option value="11">11</option>';
        $output .= '</select>';
    $output .= '</li>';

    return $output;
}

/**
 * @param $w_id
 * @param $output
 * @return string
 */
function wineListCaseQualitySelect($w_id, $output)
{
    $output .= '<li>';
        $output .= '<select class="form-control" name="' . $w_id . '[case_quantity]" id="case-qty-' . $w_id . '" />';
        $output .= '<option selected value="0">Select Quantity</option>';
        $output .= '<option value="1">1</option>';
        $output .= '<option value="2">2</option>';
        $output .= '<option value="3">3</option>';
        $output .= '<option value="4">4</option>';
        $output .= '<option value="5">5</option>';
        $output .= '<option value="6">6</option>';
        $output .= '<option value="7">7</option>';
        $output .= '<option value="8">8</option>';
        $output .= '<option value="9">9</option>';
        $output .= '<option value="10">10</option>';
        $output .= '<option value="11">11</option>';
        $output .= '</select>';
    $output .= '</li>';

    return $output;
}

/*============================================================================
 *
 *      Subjects (Top level menu) and page Functions
 */


/**
 * @param bool $category
 * @return mixed
 */
function find_all_wines($category = null)
{

    $query = "SELECT * FROM wine ";
    if ($category) {
        $query .= "WHERE `colour`= '{$category}' ";
    }
    $query .= "ORDER BY id ASC ";
    $result = executeQuery($query);
    confirm_query($result);

    return $result;
}

/**
 * Find All Wines
 * @return mixed
 */
function find_all_subjects()
{

    $query = "SELECT * FROM subjects ";
    $query .= "ORDER BY id ASC ";
    $result = executeQuery($query);
    confirm_query($result);

    return $result;
}


/**
 * @param $table_name
 * @return bool|mysqli_result
 */
function findAll($table_name)
{

    $query = "SELECT * ";
    $query .= "FROM {$table_name} ";
    $data_set = executeQuery( $query);
    confirm_query($data_set);

    return $data_set;
}


/**
 * @param string $table_name
 * @param int $id
 * @return bool|array $row
 */
function findById($table_name, $id)
{

    $query = "SELECT * FROM {$table_name} ";
    $query .= "WHERE id = '{$id}' ";

    $result = executeQuery($query);
    confirm_query($result);

    return ($row = $result->fetch_assoc()) ? $row : null;
}

/**
 * @param $table_name
 * @param $id
 * @return bool|mysqli_result
 */
function find_all_data_id($table_name, $id)
{

    $query = "SELECT * ";
    $query .= "FROM {$table_name} ";
    $query .= "WHERE `id` = {$id} ";
    $data_set = executeQuery($query);

    //confirm_query($data_set);

    return $data_set;
}

/**
 *
 * @param int $id
 * @return array $subject Contains all attributes of single subject
 *         or null
 */
function find_subject_by_id($id)
{
    if ($id === null) {
        return null;
    }

    global $mysqli;

    $safe_id = mysqli_real_escape_string($mysqli, $id);

    $query = "SELECT DISTINCT * FROM subjects ";
    $query .= "WHERE id = {$safe_id} ";
    $query .= "LIMIT 1";
    // echo $query;
    $result = executeQuery($query);
    confirm_query($result);

    if ($subject = $result->fetch_assoc()) {
        return $subject;
    } else {
        return null;
    }
}

/**
 *
 * @param int $id
 * @return array $subject Contains all attributes of single subject or null
 *
 */
function find_dist_centre_by_id($id)
{
    if ($id === null) {
        return null;
    }

    global $mysqli;

    $safe_id = mysqli_real_escape_string($mysqli, $id);

    $query = "SELECT * FROM distribution_centre ";
    $query .= "WHERE DIST_CENTRE_ID = {$safe_id} ";
    $query .= "LIMIT 1";
    // echo $query;
    $result = executeQuery($query);
    confirm_query($result);

    if ($distribution_centre = $result->fetch_assoc()) {
        return $distribution_centre;
    } else {
        return false;
    }
}


/**
 * @param $d_id
 * @param $w_id
 * @return array|bool
 */
function find_dist_stock($d_id, $w_id)
{
    if ($d_id === null || $w_id === null) {
        return null;
    }

    global $mysqli;

    $safe_dist_id = mysqli_real_escape_string($mysqli, $d_id);
    $safe_wine_id = mysqli_real_escape_string($mysqli, $w_id);


    $query = "SELECT * FROM stock ";
    $query .= "WHERE `DISTRIBUTION_CENTRE_ID` = '{$safe_dist_id}' ";
    $query .= "AND `WINE_ID` = '{$safe_wine_id}' ";
    $query .= "LIMIT 1";
    $result = executeQuery($query);
    confirm_query($result);

    if ($stock = $result->fetch_assoc()) {
        return $stock;
    } else {
        return false;
    }
}

/**
 * Find all subject from the database
 * @return mysqli_result $result
 */
function find_header_menu_subjects()
{
    $query = "SELECT * FROM subjects ";
    $query .= "WHERE subjects.visible = 1 AND subjects.position = 'header_menu' ";
    $query .= "ORDER BY subjects.sub_order ASC ";
    $result = executeQuery($query);
    confirm_query($result);

    return $result;
}

/**
 *
 * @param int $id
 * @return array $result
 */
function find_page_by_id($id)
{
    if ($id === null) {
        return null;
    }

    global $mysqli;

    $safe_id = mysqli_real_escape_string($mysqli, $id);

    $query = "SELECT DISTINCT * FROM page ";
    $query .= "WHERE id = {$safe_id} ";
    $query .= "LIMIT 1";
    // echo $query;
    $result = executeQuery($query);
    confirm_query($result);

    if ($page = $result->fetch_assoc()) {
        return $page;
    } else {
        return null;
    }
}

/**
 * Description
 * @param int $id
 * @return array $result
 */
function find_page_content_by_id($id)
{
    if ($id === null) {
        return null;
    }

    global $mysqli;

    $safe_id = mysqli_real_escape_string($mysqli, $id);

    $query = "SELECT DISTINCT * FROM page ";
    $query .= "WHERE id = {$safe_id} ";
    $query .= "LIMIT 1";
    // echo $query;
    $result = executeQuery($query);
    confirm_query($result);

    if ($page = $result->fetch_assoc()) {
        return $page['content'];
    } else {
        return null;
    }
}

/**
 * Find pages that belong to one subject
 * @param int $subject_id
 * @return mysqli_result $sub_pages
 */
function find_pages_for_subject($subject_id)
{
    global $mysqli;

    $safe_subject_id = mysqli_real_escape_string($mysqli, $subject_id);
    $query = "SELECT * FROM page ";
    $query .= "WHERE subject_id = {$safe_subject_id} ";
    $query .= "ORDER BY page_order ASC ";
    // SELECT, SHOW, DESCRIBE or EXPLAIN queries mysqli_query()
    // will return a mysqli_result object
    $sub_pages = executeQuery($query);
    confirm_query($sub_pages);

    return $sub_pages;
}

/**
 *
 */
function getCurrentPage()
{
    if (isset($_GET['subject'])) {
        return findById('subjects', $_GET['subject']);
    } elseif (isset($_GET['page'])) {
        return findById('page', $_GET['page']);
    }
    return null;
}

/*============================================================================
 *
 *      Build part of HTML Functions
 */

/**
 * Build header navigation based on $_GET parameter input
 * @return string $output Concatenated html tags and text
 */
function getNavigation()
{
    $output = '<ul class="nav navbar-nav"><li ';
    // Home page is active if no get params are sent
    if (!getCurrentPage()) {
        $output .= 'class="active"';
    }
    $output .= '><a href="index.php">Home</a></li>';

    $output .= createNavigationItems();
    $output .= createNavigationAuthItem();
    $output .= '</ul>';

    return $output;
}

/**
 * @return string $output
 */
function createNavigationItems()
{
    $output = '';
    $visible_subjects = find_header_menu_subjects();

    while ($visible_subjects && $subject = mysqli_fetch_array($visible_subjects)) {

        // Check if current menu item have related pages
        if (haveSubPage($subject['id'])) {
            $output .= createNavigationSubItem($subject);
        } else {
            $output .= createNavigationTopLeveltem($subject);
        }
    }

    return $output;
}

/**
 * @param $subjectId
 * @return bool
 */
function haveSubPage($subjectId) {
    // Get Related Pages
    $sub_pages = find_pages_for_subject($subjectId);
    if (mysqli_num_rows($sub_pages) !== 0) {
        return true;
    }
    return false;
}

/**
 *
 * @param array $subject
 * @return string
 */
function createNavigationTopLeveltem($subject)
{
    $output = '<li ';
    if (isActivePage($subject['id'])) {
        $output .= 'class="active"';
    }
    $output .= '>';
    $output .= '<a href="site.php?subject='. urldecode($subject['id']) . '">';
    $output .= $subject['title'];
    $output .= '</a></li>';

    return $output;
}

/**
 * Build a dropdown navigation item if subject have linked pages
 * Refactoring navigation function into smaller bits
 * @param $subject
 * @return string $output
 */
function createNavigationSubItem($subject)
{
    $subPages = find_pages_for_subject($subject['id']);
    $output = '<li class="dropdown"> ';
    $output .= '<a href="#" class="dropdown-toggle" data-toggle="dropdown">';
    $output .= $subject['title'];
    $output .= '<b class="caret"></b></a>';
    $output .= '<ul class="dropdown-menu">';

    while ($page = mysqli_fetch_array($subPages)) {
        // add related pages in dropdown menu
        $output .= '<li><a href="site.php?page=' . urldecode($page['id']) . '">';
        $output .= $page['title'];
        $output .= '</a></li>';
    }

    $output .= '</ul></li>';

    return $output;
}

/**
 * Create Login/Logout Button based on the $_SESSION Variable
 * @return string $output
 */
function createNavigationAuthItem()
{
    $output = '<li ';
    // @TODO Refactor this for automatically getting login/logout page id
    if (isActivePage(27) || isActivePage(28)) {
        $output .= 'class="active"';
    }

    if (!isLoggedIn()) {
        $output .= '><a href="login.php">Login</a></li>';
    } else {
        $output .= '><a href="page/logout.php">Logout </a></li>';
    }

    return $output;
}

/**
 * @param int $pageId
 * @return bool
 */
function isActivePage($pageId = 0) {
    $current_page = getCurrentPage();
    return ($current_page && $current_page['id'] == $pageId) ? true : false;
}

/**
 *  Show search results for specified page
 *  Current works only on wine table,
 *  @TODO Needs refactoring to allow custom column names using $columns array.
 *
 * @param  string $search_table
 * @param  string $path
 * @param  array $columns
 * @return string $search_output
 */
function show_search_results($search_table, $path = null, $columns = null)
{
    if (isset($_POST['search'])) {

        if ($columns) {
            // Build sql query using column name array
        }
        global $mysqli;
        $search_string = '%' . mysqli_escape_string($mysqli, $_POST['search']) . '%';

        $sql = "SELECT id, name, colour, price FROM `{$search_table}` ";
        $sql .= "WHERE `id` LIKE '{$search_string}' ";
        $sql .= "OR `name` LIKE '{$search_string}' ";
        $sql .= "OR `colour` LIKE '{$search_string}' ";
        $sql .= "OR `description` LIKE '{$search_string}' ";
        $sql .= "OR `indicator` LIKE '{$search_string}' ";
        $sql .= "OR `style` LIKE '{$search_string}' ";
        $sql .= "OR `type` LIKE '{$search_string}' ";
        $sql .= "OR `size` LIKE '{$search_string}' ";
        $sql .= "OR `bottle_per_case` LIKE '{$search_string}' ";
        $sql .= "OR `price` LIKE '{$search_string}' ";
        $sql .= "OR `case_price` LIKE '{$search_string}' ";
        $sql .= "OR `available` LIKE '{$search_string}' ";
        $sql .= "OR `country` LIKE '{$search_string}' ";

        if ($result = executeQuery($sql)) {
            $_SESSION['message'] = 'Search Results!';
            $_SESSION['message_type'] = 'success';
            $search_output = '<ul class="wine-list">';
            while ($row = $result->fetch_assoc()) {
                $search_output .= '<li><a href="' . $path . 'site.php?subject=26#' . $row['id'] . '">';
                $search_output .= $row['name'] . ' ' . $row['colour'] . ' ' . $row['price'];
                $search_output .= '</a></li>';
            }
            $search_output .= '</ul>';

            echo($search_output);
        } else { // query failed
            $_SESSION['message'] = "Error. Query failed: (" . $mysqli->errno . ") " . $mysqli->error;
            $_SESSION['message_type'] = 'warning';
        }
    }
}

/**
 * @param $sql
 * @return mixed
 */
function executeQuery($sql)
{
    global $mysqli;

    return $mysqli->query($sql);
}

/**
 * Print Footer with variable path to accomodate different directories
 * @param string $path
 * @return string $output
 */
function getFooter($path)
{
    $output = '<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>';
    $output .= '<script src="' . $path . 'js/bootstrap.min.js"></script>';
    $output .= '<script src="' . $path . 'js/docs.min.js"></script>';
    $output .= '</body></html>';

    return $output;
}


/*============================================================================
 *
 *      Form Control Queries
 */

/**
 * Find all distribution Centres
 * @return MySQL Object $resutlt
 */
function find_all_dist_centres()
{
    $query = "SELECT * FROM distribution_centre ";
    $query .= "ORDER BY dist_centre_id ASC ";

    $result = executeQuery($query);
    confirm_query($result);

    return $result;
}


/*============================================================================
 *
 *      Form Validation Functions
 */

/**
 * Make field names more visually appealing
 * @param string $fieldName
 * @param boolean $capitalise
 * @return string $fieldName Capitalised without underscores
 */
function fieldNameAsText($fieldName, $capitalise = true)
{
    $fieldName = str_replace("_", " ", $fieldName);
    $fieldName = strtolower($fieldName);
    if ($capitalise == true) {
        $fieldName = ucfirst($fieldName);
    }
    return $fieldName;

}

/**
 * Check if value was provided
 * @param string $value
 * @return boolean
 */
function has_presence($value)
{
    return isset($value) && $value !== "";
}

/**
 * Validate presence in all fields and
 * add errors to the global variable
 * @param array $required_fields
 * @return void
 */
function validate_presences($required_fields)
{
    global $errors;
    foreach ($required_fields as $field) {
        $value = trim($_POST[$field]);
        if (!has_presence($value)) {
            $errors[$field] = fieldNameAsText($field) . " can't be blank";
        }
    }
}

/**
 * Check if the string does not exceed max lenght
 * @param string $value
 * @param int $max
 * @return boolean
 */
function has_max_length($value, $max)
{
    return strlen($value) <= $max;
}


/**
 * Valida lenghts in all fields and
 * add errors to the global variable
 *
 * @param array $fields_with_max_lengths
 * @return void
 */
function validate_max_lengths($fields_with_max_lengths)
{
    global $errors;
    foreach ($fields_with_max_lengths as $field => $max) {
        $value = trim($_POST[$field]);
        if (!has_max_length($value, $max)) {
            $errors[$field] = fieldNameAsText($field) . " is to long";
        }
    }
}

/*============================================================================
 *
 *      Query Execution
 */

/**
 * @param $query
 * @param $table_name
 */
function saveInformation($query, $table_name)
{
    global $mysqli;
    if (executeQuery($query)) {
        $_SESSION['message'] = 'Success! Information have been saved!';
        $_SESSION['message_type'] = 'success';
        redirect_to('form-' . $table_name . '-view.php');
    } else {
        $_SESSION['message'] = "Error. Query failed: (" . $mysqli->errno . ") " . $mysqli->error;
        $_SESSION['message_type'] = 'warning';
    }
}


/*============================================================================
 *
 *      Errors and messages
 */


/**
 * Show validation errors, else output success message
 * @return string $output
 */
function form_errors()
{
    $output = '';
    if (isset($_SESSION["errors"])) {
        $errors = $_SESSION["errors"];

        $_SESSION['errors'] = null;

        // return $errors;
    }

    if (!empty($errors)) {

        $output .= '<div class="alert alert-danger" id="selectCodeNotificationArea">';
        $output .= 'Please fix the following errors:';
        $output .= '<ul>';
        foreach ($errors as $key => $error) {
            $output .= '<li>' . $error . '</li>';
        }
        $output .= '</ul>';
        $output .= '</div>';
    }
    if (isset($_SESSION['message']) && isset($_SESSION['message_type'])) {
        $msg = htmlentities($_SESSION['message']);
        $type = htmlentities($_SESSION['message_type']);

        // Cear
        $_SESSION['message'] = null;
        $_SESSION['message_type'] = null;

        $output .= '<div class="alert alert-' . $type . ' " id="selectCodeNotificationArea">' . $msg;
        $output .= '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>';

        // Hide only success messages after 5s
        if ($type == 'success') {
            $output .= '<script>';
            $output .= 'function showAlert() {$("#selectCodeNotificationArea").addClass("fade hide");}';
            $output .= 'window.setTimeout(function () {showAlert();}, 5000);';
            $output .= '</script>';
        }
    }

    return $output;
}

/**
 * alert message
 * @return string $output
 */
function alert_msg()
{
    if (isset($_SESSION['message']) && isset($_SESSION['message_type'])) {
        $msg = htmlentities($_SESSION['message']);
        $type = htmlentities($_SESSION['message_type']);

        // Cear
        $_SESSION['message'] = null;
        $_SESSION['message_type'] = null;

        $output = '<div class="alert alert-' . $type . ' " id="selectCodeNotificationArea">' . $msg;
        $output .= '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>';
        $output .= '<script>';
        $output .= 'function showAlert() {$("#selectCodeNotificationArea").addClass("fade hide");}';
        $output .= 'window.setTimeout(function () {showAlert();}, 5000);';
        $output .= '</script>';

        return $output;
    } else {
        return null;
    }

}


/*============================================================================
 *
 *      User Authentication and Encryption Functions
 */

/**
 * Generate salt for password
 * @param int $length
 * @return type
 */
function generate_salt($length)
{
    $unique_random_string = md5(uniqid(mt_rand(), true));

    // Valid characters fro a salt are [a-zA-Z0-9./]
    $base64_string = base64_encode($unique_random_string);

    // But not '+' which is valid in base64 encoding
    $modifiend_based64_string = str_replace('+', '.', $base64_string);

    // Truncate string to the correct length
    $salt = substr($modifiend_based64_string, 0, $length);

    return $salt;
}

/**
 * Encrypt password using generated salt, blowfish method
 * @param string $password
 * @return string $hash
 */
function password_encrypt($password)
{
    // Blowfish format with cost of 10
    $hash_format = "$2y$10$";
    // At least 22 char
    $salt_length = 22;

    $salt = generate_salt($salt_length);
    /** @var string $salt */
    $format_and_salt = $hash_format . $salt;
    $hash = crypt($password, $format_and_salt);

    return $hash;
}

/**
 * Check the encrypted password provided with existing one to match
 * @param string $password
 * @param string $existing_hash
 * @return boolean
 */

function password_check($password, $existing_hash)
{
    // Existing hash contains format and salt at start
    $hash = crypt($password, $existing_hash);

    return $hash === $existing_hash ? true : false;

}

/**
 * Login User
 * @param string $username
 * @param string $password
 * @param string $userGroup
 * @return string
 */
function attempt_login($username = null, $password, $userGroup = 'admin')
{
    if ($username === null) {
        return false;
    }
    $user = find_user_by_username($username, $userGroup);
    if ($user) {
        return password_check($password, $user['PASSWORD']) ? $user : false;
    }
    return false;
}


/**
 * @param null $username
 * @param string $userGroup
 * @return bool|string Contains all attributes of admin or false
 */
function find_user_by_username($username, $userGroup)
{

    global $mysqli;

    $safe_username = mysqli_real_escape_string($mysqli, $username);

    $query = "SELECT * FROM {$userGroup} ";
    $query .= "WHERE USERNAME = '{$safe_username}' ";
    $query .= "LIMIT 1 ";
    $result = executeQuery($query);
    confirm_query($result);

    return ($user = $result->fetch_assoc()) ? $user : false;
}


/**
 * Check if the user is authenticated
 * @return boolean
 */
function isAdmin()
{
    return ($_SESSION["adminAuth"] === 2 && isset($_SESSION['adminUsername'])) ? true : false;;
}

/**
 * Check if the user is authenticated
 * @return boolean
 */
function isLoggedIn()
{
    if (!isset($_SESSION['auth'])) {
        return false;
    }

    return ($_SESSION["auth"] === 1 && isset($_SESSION['username'])) ? true : false;

}

/**
 * Check if this is restricted page
 * @param string $view_permission
 * @param string $path
 */
function checkAccessPermissions($view_permission = 'admin', $path = null)
{
    if ($view_permission ===  "public") return;

    if ($view_permission == 'place-order' && !isLoggedIn()) {
        $_SESSION['message'] = 'Please Log In';
        $_SESSION['message_type'] = 'warning';
        $_SESSION['shopping_cart']['order_url'] = 'page/place-order.php';
        redirect_to($path . 'login.php');
    } elseif ($view_permission == 'admin' && !isAdmin()) {
        $_SESSION['message'] = 'Please Log In';
        $_SESSION['message_type'] = 'warning';
        redirect_to($path . 'page/admin-login.php');
    }
}


/*============================================================================
 *
 *      Shopping Cart Functions
 */


/**
 * Process the form and add the contents to the session
 */
function saveWineToSession()
{
    foreach ($_POST as $wine_id => $quantity_array) {

        if (is_array($quantity_array)
            && ($quantity_array['bottle_quantity'] > 0
                || $quantity_array['case_quantity'] > 0)) {
            saveWineDetailsToSession($wine_id);
            $_SESSION['wines'][$wine_id]['quantity'] = $quantity_array;
            calculate_subtotal($wine_id);
        }
    }
}

/**
 * @param int $wine_id
 */
function saveWineDetailsToSession($wine_id)
{
    $wine = findById('wine', $wine_id);

    foreach ($wine as $key => $inner_value) {
        $_SESSION['wines'][$wine_id][$key] = $inner_value;
    }


}

/**
 * Calculate sub total for the specified item
 * @param int $wineId
 * @return float $subtotal

 */
function calculate_subtotal($wineId)
{
    $wine_id = $_SESSION['wines'][$wineId]['id'];
    $btl_price = (float)$_SESSION['wines'][$wineId]['price'];
    $wine_case_price = (float)$_SESSION['wines'][$wineId]['case_price'];
    $btl_qty = (int)$_SESSION['wines'][$wineId]['quantity']['bottle_quantity'];
    $wine_case_qty = (int)$_SESSION['wines'][$wineId]['quantity']['case_quantity'];
    $subtotal = ($btl_price * $btl_qty) + ($wine_case_price * $wine_case_qty);

    $_SESSION['wines'][$wine_id]['subtotal'] = number_format((float)$subtotal, 2, '.', '');

}

/**
 * Flat Rate Shipping
 * @param int $shipping_rate
 * @return int $shipping_rate
 */
function calculate_shipping($shipping_rate = 10)
{
    $shipping_cost = number_format((float)$shipping_rate, 2, '.', '');
    if (!isset($_SESSION['shopping_cart']['shipping'])) {
        $_SESSION['shopping_cart']['shipping'] = $shipping_cost;
    }
    return $shipping_cost;

}

/**
 * [calculate_total_before_tax Sub Total of shopping basket ]
 * @return int $total_before_tax;
 */
function calculate_total_before_tax()
{
    $total_before_tax = 0;
    if (isset($_SESSION['wines'])) {
        foreach ($_SESSION['wines'] as $wine_id => $wine) {
            $total_before_tax += $wine['subtotal'];
        }
    }

    return $total_before_tax;

}

/**
 * Add tax to the shopping cart sub total
 *
 * @param integer $tax_percent
 * @return float Formated float number
 */
function add_tax($tax_percent = 20)
{
    $total_before_tax = calculate_total_before_tax();

    // Add Shipping Cost
    if (isset($_SESSION['shopping_cart']['shipping'])) {
        $total_before_tax += $_SESSION['shopping_cart']['shipping'];
    }
    // VAT on all items and shipping
    $tax_only = $total_before_tax * $tax_percent / 100;

    $_SESSION['shopping_cart']['tax'] = $tax_only;

    return number_format((float)$tax_only, 2, '.', '');

}

/**
 * Add everything up to show shopping cart total
 * @return float $total
 */
function calculate_order_total()
{
    $order_total = calculate_total_before_tax();

    if (isset($_SESSION['shopping_cart']['shipping'])) {
        $order_total += $_SESSION['shopping_cart']['shipping'];
    }
    if (isset($_SESSION['shopping_cart']['tax'])) {
        $order_total += $_SESSION['shopping_cart']['tax'];
    }
    $total = number_format((float)$order_total, 2, '.', '');
    $_SESSION['shopping_cart']['order_total'] = $total;

    return $total;
}