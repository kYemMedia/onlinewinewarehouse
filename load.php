<?php
   /**
    *
    * Bootstrap file for setting the ABSPATH constant
    * and loading the include files
    *
    */
   ob_start();
   /** Define ABSPATH as this file's directory */
   define( 'ABSPATH', dirname(__FILE__) . '/' );

   if ( file_exists( ABSPATH . 'helper/db.php') ) {

    require_once( ABSPATH . 'helper/db.php' );

   }

   if ( file_exists( ABSPATH . 'helper/functions.php') ) {

    require_once( ABSPATH . 'helper/functions.php' );

   }

   if ( file_exists( ABSPATH . 'vendor/autoload.php') ) {

    require_once( ABSPATH . 'vendor/autoload.php' );

   }

   if ( file_exists( ABSPATH . 'helper/PHPMailer/PHPMailerAutoload.php') ) {

    require_once( ABSPATH . 'helper/PHPMailer/PHPMailerAutoload.php' );

   }


   if ( file_exists( ABSPATH . 'layout/header.php') ) {

    require_once( ABSPATH . 'layout/header.php' );

   }
