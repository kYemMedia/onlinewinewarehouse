<div class="navbar-wrapper">
    <div class="container">

        <div class="navbar navbar-inverse navbar-static-top " role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/">OWW</a>
                </div>
                <?php echo getNavigation(); ?>
                <form class="navbar-form navbar-right" action="<?php echo $path; ?>site.php" method="POST">
                    <input type="text" name="search" class="form-control" placeholder="Search...">
                </form>
            </div>
        </div>
    </div>
</div>