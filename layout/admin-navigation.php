<div class="navbar-wrapper">
    <div class="container">

        <div class="navbar navbar-inverse navbar-static-top" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php echo $path ?>index.php">OWW</a>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="<?php echo $path; ?>admin.php">Dashboard</a></li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                Dist. Centres<span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="<?php echo $path; ?>page/form-distribution_centre-create.php">Create</a>
                                </li>
                                <li><a href="<?php echo $path; ?>page/form-distribution_centre-view.php">Manage</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                Wines<span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="<?php echo $path; ?>page/form-wine-create.php">Create</a></li>
                                <li><a href="<?php echo $path; ?>page/form-wine-view.php">Manage</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                Stock<span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="<?php echo $path; ?>page/form-stock-create.php">Create</a></li>
                                <li><a href="<?php echo $path; ?>page/form-stock-view.php">Manage</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                Admins<span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="<?php echo $path; ?>page/form-admin-create.php">Create</a></li>
                                <li><a href="<?php echo $path; ?>page/form-admin-view.php">Manage</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                Menus<span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="<?php echo $path; ?>page/form-subjects-create.php">Create</a></li>
                                <li><a href="<?php echo $path; ?>page/form-subjects-view.php">Manage</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                Pages<span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="<?php echo $path; ?>page/form-page-create.php">Create</a></li>
                                <li><a href="<?php echo $path; ?>page/form-page-view.php">Manage</a></li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right" role="form">
                        <li><a href="<?php echo $path; ?>page/admin-logout.php">Logout </a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>