<?php

// Enable sessions on all html pages
session_set_cookie_params('3600'); // 1 hour
session_start();

// Use $_GET variable to identify subject or page
$page = getCurrentPage();

// Make sure path is set
if (!isset($path)) {
    $path = null;
}
if (!isset($view_permission)) {
    $view_permission = 'admin';
}
checkAccessPermissions($view_permission, $path);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <!-- <meta http-equiv="X-UA-Compatible" content="IE=edge"> -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Online Wine Warehouse - A wide range of wines available online">
    <meta name="author" content="Group 30">
    <link rel="shortcut icon" href="<?php echo $path; ?>images/ico/favicon.ico">

    <title><?php echo $page_title; ?></title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo $path; ?>css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo $path; ?>css/datepicker3.css" rel="stylesheet">
    <!-- Custom styles -->
    <link href="<?php echo $path; ?>css/carousel.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="<?php echo $path; ?>js/bootstrap.min.js"></script>
    <script src="<?php echo $path; ?>js/docs.min.js"></script>
    <script src="<?php echo $path; ?>js/bootstrap-datepicker.js"></script>

</head>

<body>